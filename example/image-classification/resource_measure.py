'''
    https://psutil.readthedocs.io/en/latest/index.html?highlight=process%20#cpu
    https://github.com/giampaolo/psutil/blob/master/scripts/nettop.py
    https://github.com/anderskm/gputil
'''
import getpass
import psutil
import GPUtil
import time
import sys
import re

def getProcess(training_process):
    '''
    pattern = r'python3' is the filter of counting your processes
    '''
    result = []
    pattern = r'python3'
    pids = [p.info['pid'] for p in psutil.process_iter(attrs=['pid', 'name', 'username']) if 'python3' in p.info['name'] and p.info['username'] == getpass.getuser()]
    for pid in pids:
        p = psutil.Process(pid)
        matchObj = re.match(pattern, p.cmdline()[0], flags=0)
        if matchObj:
            result.append(p)
    while len(result) < training_process:
        result.clear()
        pids = [p.info['pid'] for p in psutil.process_iter(attrs=['pid', 'name', 'username']) if 'python3' in p.info['name'] and p.info['username'] == getpass.getuser()]
        for pid in pids:
            p = psutil.Process(pid)
            matchObj = re.match(pattern, p.cmdline()[0], flags=0)
            if matchObj:
                result.append(p)
    return result

if __name__ == '__main__':

    '''
    sys.argv[1]:
        if 2: co-locate, there are 2 processes(the worker, the server)
        if 1: separate, there is only 1 process(worker or server)
    ts = int(round(time.time()*1000)) #毫秒
    '''
    print("{")
    '''
        info of cpu, memory
    '''
    print('''"info":{''')
    print('''"CPU physical count":%d, "CPU logical count":%d, "mem":%d, "swap":%d'''%(\
        psutil.cpu_count(logical=False), \
        psutil.cpu_count(), \
        psutil.virtual_memory().total, \
        psutil.swap_memory().total ))
    print("},")
    print('''"gpus":[''')
    FLAG = False
    for gpu in GPUtil.getGPUs():
        if FLAG:
            print(",")
        FLAG = True
        print("{")
        print('''"gpu":%d, "memoryTotal":%d, "driver":"%s"'''%(\
            gpu.id, \
            gpu.memoryTotal, \
            gpu.driver))
        print("}")
    print("],")

    pids = getProcess(int(sys.argv[1]))
    print('''"pids":[''')
    FLAG = False
    for p in pids:
        if FLAG:
            print(",")
        FLAG = True
        print("{")
        print('''"role":"%s", "pid":%d, "environ":"%s", "cmdline":"%s"'''%(\
            p.environ()['DMLC_ROLE'], \
            p.pid, \
            p.environ(), \
            p.cmdline() ))
        print("}")
    print("],")

    print('''"training":[''')
    #for i in range(10):
    TRAIN = True
    FLAG = False
    while TRAIN:
        if FLAG:
            print(",")
        FLAG = True
        print("{")

        '''
            gpu.memoryUtil #MiB=2^20 Bytes
            gpu.load # % Percent of time over the past sample period
        '''
        print('''"gpus":[''')
        FLAG2 = False
        for gpu in GPUtil.getGPUs():
            ts = int(round(time.time()*1000))
            id = gpu.id
            load = gpu.load
            mem = gpu.memoryUtil
            if FLAG2:
                print(",")
            FLAG2 = True
            print('''{"ts":%d, "gpu":%d, "gload":%f, "gmem":%f}'''%(ts, id, load, mem,))
        print("],")

        '''
            /1024/1024 =MB # rss =>res, vms =>vms
        '''
        print('''"pids":[''')
        FLAG1 = False
        for p in pids:
            try:
                #psutil.pid_exists(p.pid)
                role = p.environ()['DMLC_ROLE']
                ts = int(round(time.time()*1000))
                id = p.pid
                cpu = p.cpu_percent(interval=None)
                mem = p.memory_info().rss
                if FLAG1:
                    print(",")
                FLAG1 = True
                print("{")
                print('''"role":"%s", "ts":%d, "pid":%d, "cpu":%f, "mem":%d'''%(role, ts, id, cpu, mem))
                print("}")
            except Exception as e:
                TRAIN = False
        print("],")

        '''
            psutil.cpu_percent(interval=0, percpu=True) # %
            psutil.net_io_counters(pernic=True, nowrap=True) # Bytes
            bytes_sent, bytes_recv, packets_sent, packets_recv, errin=0, errout=0, dropin=0, dropout=0
        '''
        print('''"net":{''')
        ts = int(round(time.time()*1000))
        net = psutil.net_io_counters(pernic=True, nowrap=True)
        print('''"ts":%d, "net":"%s"'''%(ts, net))
        print("}")
        print("}")

        #time.sleep(0.001) # 1 ms
    print("]}")

