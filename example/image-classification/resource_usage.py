#!/usr/bin/python

import os
import sys
import time
import socket
import argparse
import threading

#applications parameters
params = {'resnet50' : '--network resnet --num-layers 50 --image-shape 3,299,299 ',
'resnet152' : '--network resnet --num-layers 152 --image-shape 3,299,299 ',
'vgg16' : '--network vgg --num-layers 16 --image-shape 3,299,299 ',
'vgg19' : '--network vgg --num-layers 19 --image-shape 3,299,299 ',
'inception-v4' : '--network inception-v4 --image-shape 3,299,299 ',
'inception-v5' : '--network inception-v5 --image-shape 3,299,299 ',
'lenet' : '--network lenet ',
'lenet1' : '--network lenet1 '}

#batchsize = {'resnet50' : [64], 'resnet152' : [16], 'inception-v4' : [32], 'vgg16' : [16], 'vgg19' : [16], 'lenet' : [64], 'inception-v5' : [16], 'lenet1' : [64]}
batchsize = {'resnet50' : [64], 'resnet152' : [1,2,4,8,16], 'inception-v4' : [1,2,4,8,16,32], 'vgg16' : [16], 'vgg19' : [16], 'lenet' : [64], 'inception-v5' : [16], 'lenet1' : [64]}

#applications name
# applications = ['resnet18', 'inception-bn', 'vgg16']
#applications = ['inception-v4', 'resnet152', 'vgg19']
# applications = ['inception-v4']
# applications = ['inception-v5']
# applications = ['resnet50']
# applications = ['resnet152']
# applications = ['vgg16']
applications = ['vgg19']
# applications = ['lenet']
# applications = ['lenet1']
#parse the parameters
def parseArgs():
	parser = argparse.ArgumentParser(
		formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        	description='measure the resource usage')
	parser.add_argument('--consis', type = str, default = 'BSP', choices = ['BSP', 'ASP'],
							     help = "consistency model, choices BSP or ASP, example: --consis='BSP'")
	parser.add_argument('--servers', type = int, nargs = '+', default = [],
							     help = 'assign servers onto machine, choice from [1,2,3,4,5,6,7,8], example: --servers $(seq 1 3)')
	parser.add_argument('--workers', type = int, nargs = '+', default = [],
							     help = 'assign workers onto machine, choice from [1,2,3,4,5,6,7,8], example: --workers $(seq 4 6), differ from servers')
	parser.add_argument('--gpus', type = int, nargs = '+', default = [],
							     help = 'choice gpus to compute, choice from [0,1], example: --gpus 0 1')
	parser.add_argument('--profiler', action = 'store_true',
							     help = 'switch on profiler or not')
	parser.add_argument('--numexamples', type = int, default = 51200, choices = [102400, 12800, 8192, 3840, 1920, 64, 768],
							     help = 'assign number of samples for training')
	parser.add_argument('--batch-size', type = int, default = 0, choices = [1, 2, 4, 8, 16, 32, 64],
							     help = 'assign number of batch size')
	parser.add_argument('--virtualenv', action = 'store_true', help = 'switch to virtualenv or not')
	parser.add_argument('--userdma', action = 'store_true', help = 'if true, use rdma, else tcp/ip')
	parser.add_argument('--benchmark', action = 'store_true', help = 'if true, measure with benchmark, else real data')
	parser.add_argument('--strongscaling', action = 'store_true', help = 'if true, update batch size strongscaling, else weak')
	parser.add_argument('--usetools', action = 'store_true', help = 'if true, use cpu, network, gpu detection tools')
	parser.add_argument('--breakdown', action = 'store_true', help = 'if true, copy file from ps machines')
	parser.add_argument('--interface', type = str, default = 'tenGE', choices = ['tenGE', 'ib'], help = 'the environment to execute')
	return parser.parse_args()

def genHosts(parsedArgs):
	if parsedArgs.interface == 'ib':
		hostsout = '\n'.join('10.0.0.' + str(i+40) for i in parsedArgs.servers + parsedArgs.workers)
	else:
		hostsout = '\n'.join('192.168.1.' + str(i+40) for i in parsedArgs.servers + parsedArgs.workers)
	hostsfile = open('hosts', 'w')
	hostsfile.write(hostsout)
	hostsfile.close()

class Measure:
	"""docstring for Measure"""
	def __init__(self, app, resultpath):
		self.thread_start = list()
		self.thread_finish = list()
		self.app = app
		self.resultpath = resultpath

	def launch_begin(self, hostid, training_process, parsedArgs):
		if parsedArgs.interface == 'ib':
			hostname = '192.168.1.' + str(hostid+40)
		else:
			hostname = '10.0.0.' + str(hostid+40)
		cmd = '''ssh %s 'nohup python /home/gbxu/image-classification/resource_measure.py %d > util_%s.log 2>&1 &' '''%(hostname,training_process, hostname)
		os.system(cmd)
		print(time.ctime(time.time()), cmd)
	def start(self, parsedArgs):
		servers_set = set(parsedArgs.servers)
		workers_set = set(parsedArgs.workers)
		for hostid in list(servers_set.union(workers_set)):
			if hostid in servers_set.intersection(workers_set): #union set
				training_process = 2
			else:
				training_process = 1
			t = threading.Thread(target = self.launch_begin, args = (hostid, training_process, parsedArgs, ))
			t.setDaemon(True)
			self.thread_start.append(t)
			t.start()
		# for t in self.thread_start:
		# 	t.join()
	def launch_finish(self, hostid, parsedArgs):
		if parsedArgs.interface == 'ib':
			hostname = '192.168.1.' + str(hostid+40)
		else:
			hostname = '10.0.0.' + str(hostid+40)
		# cmd = r'''ssh %s " kill \$(ps -aux | grep ifstat | awk '{print \$2}')"'''%(hostname)
		#cmd = r'''ssh %s "killall python"'''%(hostname)
		#os.system(cmd)
		cmd = "scp gbxu@%s:/home/gbxu/*.log %s"%(hostname, self.resultpath)
		os.system(cmd)
		cmd = "ssh %s 'rm -f /home/gbxu/*.log'"%(hostname)
		os.system(cmd)
		print(time.ctime(time.time()), 'launch_finish:', hostname)
	def finish(self, parsedArgs):
		servers_set = set(parsedArgs.servers)
		workers_set = set(parsedArgs.workers)
		for hostid in list(servers_set.union(workers_set)):
			t = threading.Thread(target = self.launch_finish, args = (hostid, parsedArgs))
			t.setDaemon(True)
			self.thread_finish.append(t)
			t.start()
		for t in self.thread_finish:
			t.join()

class trainingApp:
	"""traing applications"""
	def __init__(self, app, curr_params, parsedArgs, resultpath):
		self.app = app
		self.params = curr_params
		self.consis_comm  = 'device_sync' if parsedArgs.consis == 'BSP' else 'async' #set the consistency model
		self.resultpath = resultpath
	def training(self, parsedArgs):
		# cmd = "mkdir -p %s/%dserver%dworkers"%(self.resultpath, len(parsedArgs.servers), len(parsedArgs.workers))
		# os.system(cmd)
		profile = "--profile --profile-worker-suffix worker.json --profile-server-suffix server.json" if parsedArgs.profiler else ""
		traindata = '-299' if 'inception' in self.app else ''
		gpuid = str(parsedArgs.gpus[0]) if len(parsedArgs.gpus) == 1 else ','.join(str(i) for i in parsedArgs.gpus)

		cmd = '''%(env)s %(rdma)s python ../../tools/launch.py -n %(numworker)d -s %(numserver)d --launcher ssh -H hosts  --sync-dst-dir ~/image-classification/  "%(rdma)s PS_VERBOSE=-1 python3 train_imagenet.py %(trainparam)s %(prof)s  --num-examples %(samples)d --kv-store dist_%(consiscomm)s  %(gpuinfo)s --num-epochs 1 %(data)s" 2>&1 | tee %(path)s/imagenet_distributed_%(appname)s_%(consisflag)s_%(time)s.log''' \
			%({ \
				'numworker':len(parsedArgs.workers),  \
				'numserver':len(parsedArgs.servers),   \
				'trainparam':self.params,   \
		 		'consiscomm':self.consis_comm,   \
		 		'appname':self.app,    \
				'consisflag':parsedArgs.consis,    \
				'time':str(time.strftime("%Y-%m-%d_%H:%M:%S", time.localtime())),    \
				'path':self.resultpath,    \
				'prof':profile,    \
				'samples':parsedArgs.numexamples,    \
				'env':"source /home/gbxu/mxnetGPU;" if parsedArgs.virtualenv else "", \
				'rdma':"DMLC_ENABLE_RDMA=1" if parsedArgs.userdma else "DMLC_ENABLE_RDMA=0",    \
				'data': '--benchmark 1' if parsedArgs.benchmark else "--data-train /data/trainData/traindata%s.rec --data-train-idx /data/trainData/traindata%s.idx"%(traindata, traindata), \
				'gpuinfo': '--gpus %s'%(gpuid) if len(parsedArgs.gpus) > 0 else '' \
			})
		print(cmd)
		os.system("echo %s > %s/command.log"%(cmd, self.resultpath))
		os.system(cmd)

def endOfonce(resultpath, parsedArgs):
	for hostid in parsedArgs.servers+parsedArgs.workers:
		if parsedArgs.interface == 'ib':
			hostname = '192.168.1.' + str(hostid+40)
		else:
			hostname = '10.0.0.' + str(hostid+40)
		cmd = r'''ssh %s "kill \$(ps -aux | grep train_imagenet | awk '{print \$2}')"'''%(hostname)
		print(cmd)
		os.system(cmd)
	if parsedArgs.profiler:
		servers_set = set(parsedArgs.servers)
		workers_set = set(parsedArgs.workers)
		for hostid in list(servers_set.union(workers_set)):
			if parsedArgs.interface == 'ib':
				hostname = '192.168.1.' + str(hostid+40)
			else:
				hostname = '10.0.0.' + str(hostid+40)
			cmd = "scp gbxu@%s:/home/gbxu/image-classification/*.json %s"%(hostname, resultpath)
			print(cmd)
			os.system(cmd)
			cmd = "ssh %s 'rm -f /home/gbxu/image-classification/*.json'"%(hostname)
			print(cmd)
			os.system(cmd)
			cmd = "scp gbxu@%s:/home/gbxu/image-classification/hosts %s"%(hostname, resultpath)
			os.system(cmd)
			print(time.ctime(time.time()), 'get hosts:', hostname)
	if parsedArgs.breakdown:
		for hostid in parsedArgs.workers+parsedArgs.servers:
			if parsedArgs.interface == 'ib':
				hostname = '192.168.1.' + str(hostid+40)
			else:
				hostname = '10.0.0.' + str(hostid+40)
			if hostname == str(socket.gethostname()):
				cmd = "mv /home/gbxu/*.txt %s"%(resultpath)
				os.system(cmd)
			else:
				cmd = "scp gbxu@%s:/home/gbxu/*.txt %s"%(hostname, resultpath)
				os.system(cmd)
				cmd = "ssh %s 'rm -f ~/gbxu/*.txt ~/gbxu/*.csv'"%(hostname)
				os.system(cmd)
	print("end of main", time.ctime(time.time()))

def initialize(curr_params, batch):
	# initialize the batch size, strongscaling or weak
	if parsedArgs.strongscaling:
		batch //= len(parsedArgs.gpus)*len(parsedArgs.workers)
		curr_params += ' --batch-size '+str(batch)
		print(curr_params)
	else:
		curr_params += ' --batch-size '+str(len(parsedArgs.gpus)*batch) # weakscaling, batch-size per GPU
		print(curr_params)
	return curr_params

if __name__ == '__main__':
	parsedArgs = parseArgs() #parse the parameters
	if parsedArgs.interface == 'ib':
		os.environ["DMLC_INTERFACE"] = "ib0"#set the network interface as infiniband
	if parsedArgs.userdma:
		os.environ["DMLC_ENABLE_RDMA"] = "1"
	genHosts(parsedArgs) #generate the hosts file for training
	basepath = '/home/gbxu/experiments_data_profile/%dserver-%dworkers*%dGPU-%s/'%(len(parsedArgs.servers), len(parsedArgs.workers), len(parsedArgs.gpus), str(parsedArgs.interface))
	for app in applications:
		curr_params = params[app]
		for batch in batchsize[app]:
			curr_params = initialize(curr_params , batch)
			resultpath = basepath + app
			if parsedArgs.strongscaling:
				resultpath += '-strong'
			else:
				resultpath += '-weak'
			resultpath = resultpath + '-' + str(parsedArgs.consis) + '-' + str(parsedArgs.numexamples) + '-' + str(batch) + '/'
			resultpath += str(time.strftime("%Y-%m-%d_%H:%M:%S", time.localtime()))
			os.system("mkdir -p %s"%(resultpath))

			# create measurement tools
			if parsedArgs.usetools:
				measure = Measure(app, resultpath)

			train = trainingApp(app, curr_params, parsedArgs, resultpath)

			#start the network, cpu, gpu tools per machine
			if parsedArgs.usetools:
				measure.start(parsedArgs)

			#start training
			train.training(parsedArgs)

			#end of main function
			endOfonce(resultpath, parsedArgs)

			#finish the network, cpu, gpu tools per machine
			if parsedArgs.usetools:
				measure.finish(parsedArgs)

