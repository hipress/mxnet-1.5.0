#!/usr/bin/python3
'''
collect cpu gpu network message from source files
'''

import os
import sys
import argparse
import re

#parse the parameters
def parseArgs():
	parser = argparse.ArgumentParser(
		formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        	description='measure the resource usage')
	parser.add_argument('--clean', action = 'store_true',
							     help = 'clean the .data files')
	return parser.parse_args()

def cpu(root, filename):
	part = filename.split('_')
	if 'cpu' in part[0]:
		file = root + '/' + filename
		cmd = "cat %s | awk '{print $9\",\"$10}' > %s/cpu_%s.csv"%(file, root, part[1])
		print(cmd)
		os.system(cmd)

def gpu(root, filename):
	part = filename.split('_')
	if 'gpu' in part[0]:
		file = root + '/' + filename
		cmd = "cat %s | grep \"0  GeForce\" -A 1 | grep \"%%\" | awk '{print $13\"\\t\"$9}' | sed 's/MiB/ /g' | sed 's/%%//g' > %s/gpu_%s.data"%(file, root, part[1])
		print(cmd)
		os.system(cmd)
def network(root, filename):
	part = filename.split('_')
	if 'network' in part[0]:
		file = root + '/' + filename
		cmd = "cat %s | awk '/^\s*[0-9]+.[0-9]*/ {print $1/1024\",\"$2/1024}' > %s/network_%s.csv"%(file, root, part[1])
		print(cmd)
		os.system(cmd)
def speedAndtime(root, filename):
	print(filename)
	part = filename.split('_')
	if 'imagenet' in part[0]:
		file = root + '/' + filename
		cmd = "cat %s"%(file)
		print(cmd)
		content = os.popen(cmd).readlines()
		speed = dict()
		timecost = dict()
		for line in content:
			x = re.search('(gpu\d+).*?Speed.*?(\d+\.\d*)', line)
			if x:
				if x.group(1) in speed:
					speed[x.group(1)].append(float(x.group(2)))
				else:
					speed[x.group(1)] = [float(x.group(2))]
			else:
				y = re.search('(gpu\d+).*?Time cost=(\d+\.\d*)', line)
				if y:
					timecost[y.group(1)] = float(y.group(2))
		output = dict()
		# timecostout = dict()
		print(speed)
		for gpuid in speed:
			try:
				output[gpuid] = [sum(speed[gpuid]) / len(speed[gpuid])]
			except KeyError:
				print('no key:', gpuid)
				continue
		for gpuid in timecost:
			try:
				output[gpuid].append(timecost[gpuid])
			except KeyError:
				print('no key:', gpuid)
				continue
		print(output)
		print(timecost)
		#output to file
		f = open(root+'/imagenet.csv', 'w')
		col = list('#')
		for gpuid in output:
			col.append(gpuid)
		f.write(','.join(col) + '\nspeed')
		speedtmp = list()
		for gpuid in output:
			f.write(','+str(output[gpuid][0]))
			speedtmp.append(output[gpuid][0])
		f.write(','+str(sum(speedtmp)) + '\ntimecost')
		timecosttmp = list()
		for gpuid in output:
			try:
				f.write(','+str(output[gpuid][1]))
				timecosttmp.append(output[gpuid][1])
			except IndexError:
				print('list index out of range')
		f.write(','+str(max(timecosttmp)))


def cleanfile(root):
	cmd = "rm -f %s/*.data"%(root)
	os.system(cmd)
	cmd = "rm -f %s/*.csv"%(root)
	os.system(cmd)

if __name__ == '__main__':
	# workernum = int(sys.argv[1])
	args = parseArgs()
	count = 0
	for root, dirs, files in os.walk('./experiments_data'):
		# print(files)
		if root != './': # cd the child dir of current path
			if len(dirs) == 0 and len(files) != 0: #the dir contains .log files
				if args.clean:
					cleanfile(root)
				else:
					print(root)
					for file in files:
						cpu(root, file)
						# gpu(root, file)
						network(root, file)
						speedAndtime(root, file)
					count += 1
	print(count)
