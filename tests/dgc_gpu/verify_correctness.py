import numpy as np
import mxnet as mx
import time

def binary(num):
    import struct
    return ''.join(bin(c).replace('0b', '').rjust(8, '0') for c in struct.pack('!f', num))

def float32_to_int32(num):
    b = binary(num)
    if b[0]=='0':
        return int(b,2)
    else:
        return -int(b[1:],2)

def get_output_size(input_size, result_num_per_thread, s_percent):
    exact_output_size = input_size * s_percent
    import math
    exact_output_size = math.ceil(exact_output_size)
    result_thread_num = math.ceil(exact_output_size/result_num_per_thread)
    true_output_size = result_thread_num * result_num_per_thread
    return int(true_output_size)

if __name__ == '__main__':

    N = 2**13
    result_num_per_thread = 10
    s_percent = 0.01
    sample_rate = 0.001
    numValue = N
    print("N={}".format(N))
    print("preparing a...")
    import random
    # a = [random.randint(0,numValue-1) for i in range(N)]
    a = [random.uniform(-1,1) for i in range(N)]
    print("a[:100]:",a[:100])


    a = np.array(a)
    print("a prepared!")
    print("preparing gpu data: input b and output c")
    b = mx.nd.array(a, ctx=mx.context.gpu(0),dtype="float32")
    c = mx.nd.array(a,ctx=mx.context.gpu(0),dtype="float32")
    print("input b and output c prepared!")
    mx.nd.contrib.dgcg(data=b,out=c)
    print("converting ctx c to numpy d...")
    d = c.asnumpy()
    print("converting ok!")

    output_size = get_output_size(N, result_num_per_thread, s_percent)
    print("output_size = {}".format(output_size))
    # exit(0)
    print("validating:")
    output_size = get_output_size(N, result_num_per_thread, s_percent)
    useable_cnt = 0
    for i in range(output_size):
        val = d[i*2]
        index = float32_to_int32(d[i*2+1])
        if (val != 0):
            useable_cnt+=1
            # print(index,val,end='\t')
            # if a[index] != val:
            if abs(a[index]-val) > 1e-4:
                print("check failed: i={}\tindex={}\tvalue={}\ta[index]={}".format(i,index,val,a[index]))
                assert(0)
    print("check pass: useable_cnt={}".format(useable_cnt))


    # sample_cnt = int(N*0.001)
    # import math
    # sample_cnt = math.ceil(N*0.001/1024)*1024
    # print("validating: sample_cnt={}".format(sample_cnt))
    # print("print d[:{}] as float".format(sample_cnt))
    # for v in d[:sample_cnt]:
    #     print(int(v),end=' ')
    # print()
    # print("print sorted value: d[{}:{}]".format(sample_cnt*2,sample_cnt*3))
    # for v in d[sample_cnt*2:sample_cnt*3]:
    #     print(int(v),end=' ')
    # print()
    
    # bulk = [0 for i in range(N+1)]
    # for v in d[:sample_cnt]:
    #     bulk[int(v)] += 1
    # for v in d[sample_cnt*2:sample_cnt*3]:
    #     bulk[int(v)]-=1
    # assert(max(bulk)==0)
    # assert(min(bulk)==0)
    # exit(0)
    # print("print d[:1048]%16 as int:")
    # for v in d[:1048]:
    #     print(float32_to_int32(v) % 16,end=' ')




