import numpy as np
import mxnet as mx
import time

output_csv = r"dgc_gpu.csv"

def binary(num):
    import struct
    return ''.join(bin(c).replace('0b', '').rjust(8, '0') for c in struct.pack('!f', num))

def float32_to_int32(num):
    b = binary(num)
    if b[0]=='0':
        return int(b,2)
    else:
        return -int(b[1:],2)
def test_time(input_size, s_percent=0.01, sample_rate = 0.001, result_num_per_thread=10):
    print("="*20)
    print("testing:\tinput_size={}\ts_percent={}\tsample_rate={}\tresult_num_per_thread={}".format(input_size,s_percent,sample_rate,result_num_per_thread))
    total_time = 0
    total_cnt = 0
    total_time_reverse = 0

    print("preparing a...")
    a = np.random.randn(input_size)
    print("a prepared!")
    print("preparing gpu data: input b and output c")
    b = mx.nd.array(a, ctx=mx.context.gpu(0),dtype="float32")
    c = mx.nd.zeros(shape=(544000),ctx=mx.context.gpu(0),dtype="float32")
    d = mx.nd.array(a, ctx=mx.context.gpu(0),dtype="float32")
    print("b and c prepared!")
    mx.nd.contrib.dgcg(data=b,s_percent=s_percent,sample_rate=sample_rate,result_num_per_thread=result_num_per_thread, out=c)
    c.wait_to_read()
    while total_cnt < 10:
        total_cnt+=1
        t1 = time.time()
        mx.nd.contrib.dgcg(data=b,s_percent=s_percent,sample_rate=sample_rate,result_num_per_thread=result_num_per_thread, out=c)
        c.wait_to_read()
        t2 = time.time()
        mx.nd.contrib.dgcrg(data=c,original_size=input_size,out=d)
        d.wait_to_read()
        t3 = time.time()
        total_time = total_time + t2 - t1
        total_time_reverse = total_time_reverse + t3 - t2

    average_time = total_time / total_cnt
    average_time_reverse = total_time_reverse / total_cnt
    with open(output_csv,'a') as f:
        line = ["dgc",input_size,s_percent,sample_rate,total_time,total_cnt,average_time]
        line = [str(v) for v in line]
        line = ','.join(line)
        f.write(line+'\n')
        line = ["dgcr",input_size,s_percent,sample_rate,total_time_reverse,total_cnt,average_time_reverse]
        line = [str(v) for v in line]
        line = ','.join(line)
        f.write(line+'\n')

    print("average_time={}".format(average_time))
    print("average_time_reverse={}".format(average_time_reverse))
 

if __name__ == '__main__':
    with open(output_csv, 'w') as f:
        attr = ["operator","input_size","s_percent","sample_rate","total_time","total_cnt","average_time"]
        line = ','.join(attr)
        f.write(line+'\n')


    max_size = 2**27
    # input_size = 32
    # input_size = 2**13
    input_size = max_size
    while input_size <= max_size:
        # for s_percent in [0.01,0.001]:
        for s_percent in [0.001]:
            for sample_rate in [0.001]:
                for result_num_per_thread in [100,10,1]:
                    test_time(input_size,s_percent=s_percent,sample_rate=sample_rate,result_num_per_thread=result_num_per_thread)
        input_size*=2

    # input_size = 2**27 = 134217728
    # s_percent =0.001
    # sample_rate = 0.001
    # result_num_per_thread = 1
    # ||||||||||||||||||||||||||||||
    # result_cnt = 134217.728
    # result_thread_num = 134218
    # output_size = 134218
    # original_num_per_thread = ceil(input_size/result_thread_num) = 1000

   
