/*
 * This program uses the host CURAND API to generate 100
 * pseudorandom floats.
 */
#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <curand.h>
#include <iostream>
#include <cstdlib> // std::atoi
#include "ZQ_CPP_LIB/time_cost.hpp"
#define CURAND_ORDERING_PSEUDO_SEEDED

#define CUDA_CALL(x) do { if((x)!=cudaSuccess) { \
    printf("Error at %s:%d\n",__FILE__,__LINE__);\
    return EXIT_FAILURE;}} while(0)
#define CURAND_CALL(x) do { if((x)!=CURAND_STATUS_SUCCESS) { \
    printf("Error at %s:%d\n",__FILE__,__LINE__);\
    return EXIT_FAILURE;}} while(0)

int main(int argc, char *argv[])
{
    // size_t n = 1<<2;
    size_t n = std::atoi(argv[1]);
    size_t i;
    curandGenerator_t gen;
    unsigned int *devData, *hostData;
    

    /* Allocate n floats on host */
    hostData = (unsigned int *)calloc(n, sizeof(unsigned int));

    /* Allocate n floats on device */
    CUDA_CALL(cudaMalloc((void **)&devData, n*sizeof(unsigned int)));

    /* Create pseudo-random number generator */
    CURAND_CALL(curandCreateGenerator(&gen,
        CURAND_RNG_PSEUDO_DEFAULT));
        
    curandSetGeneratorOrdering(
        gen,
        // CURAND_ORDERING_PSEUDO_SEEDED
        CURAND_ORDERING_PSEUDO_SEEDED
    );
        
    // CURAND_CALL(curandCreateGenerator(&gen, 
    //     CURAND_RNG_PSEUDO_SEEDED));

    /* Set seed */
    CURAND_CALL(curandSetPseudoRandomGeneratorSeed(gen,
                1234ULL));

    /* Generate n floats on device */
    zq_cpp_lib::get_start_time();
    CURAND_CALL(curandGenerate(gen, devData, n));
    cudaDeviceSynchronize();
    zq_cpp_lib::get_end_time();
    std::cout<<"Generate " << n << " stocastic numbers cost time(us):"
        << zq_cpp_lib::get_cost_time_by_us() << std::endl;
    

    /* Copy device memory to host */
    CUDA_CALL(cudaMemcpy(hostData, devData, n * sizeof(unsigned int),
        cudaMemcpyDeviceToHost));

    /* Show result */
    for(i = 0; i < 128; i++) {
        printf("%u ", hostData[i] & 0xF);
    }
    printf("\n");

    /* Cleanup */
    CURAND_CALL(curandDestroyGenerator(gen));
    CUDA_CALL(cudaFree(devData));
    free(hostData);
    return EXIT_SUCCESS;
}