#include <iostream>
#include <random>
#include "ZQ_CPP_LIB/time_cost.hpp"
int main(int argc, char** argv){
    std::default_random_engine e(0);
    std::uniform_int_distribution<int> uniform_dist(1,1<<27);
    // const int N = 1<<17;
    int N = std::atoi(argv[1]);
    int* a = new int[N];
    // int a[N];
    zq_cpp_lib::get_start_time();
    for (auto i = 0; i < N; i++){
        a[i] = uniform_dist(e);
    }
    zq_cpp_lib::get_end_time();
    for (auto i = 0; i < 128; i++){
        std::cout<<a[i]<<' ';
    }
    std::cout<<std::endl;
    std::cout<<"cost time(us):\t"<<zq_cpp_lib::get_cost_time_by_us()<<std::endl;
    return 0;
}