import numpy as np
import mxnet as mx
import time

def binary(num):
    import struct
    return ''.join(bin(c).replace('0b', '').rjust(8, '0') for c in struct.pack('!f', num))

def float32_to_int32(num):
    b = binary(num)
    if b[0]=='0':
        return int(b,2)
    else:
        return -int(b[1:],2)

if __name__ == '__main__':
    cnt = 1000000
    cnt = 32
    a = [i - cnt//2 for i in range(cnt)]
    a = np.array(a,dtype=np.float32)
    #a = np.array([0,1,2,3,4,5,6,7,8,9,10,11,12,12,12])
    print("origin data:",a)
    b = mx.nd.array(a)
    print("input data before dgc:", a)
    c = mx.nd.contrib.dgc(data=b,s_percent=0.001)
    d = c.asnumpy()

    print("output data:",c)
    for i in range(10):
        print("value:\t",d[i*2])
        print("index:\t",float32_to_int32(d[i*2+1]))
    true_size = float32_to_int32(d[-1])
    print("true size:",true_size)
    e = b.asnumpy()
    print("input data after dgc:",e)
    d = d[:true_size*2+1];
    original_size = float32_to_int32(d[-1])
    d = mx.nd.array(d)
    f = mx.nd.contrib.dgcr(data=d,original_size=original_size)
    f = f.asnumpy()
    print("decode data:", f)
    print("original data:", a)
    exit(0)
    for i in range(len(a)):
        if f[i]!=a[i]:
            print("Error! i={},f[i]={},a[i]={}".format(i,f[i],a[i]))
    




