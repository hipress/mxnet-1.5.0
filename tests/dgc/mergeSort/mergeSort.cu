#include <assert.h>
#include <cooperative_groups.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cuda_runtime.h>

typedef unsigned int uint;

#define SHARED_SIZE_LIMIT 1024U
#define     SAMPLE_STRIDE 128

template <typename T>
uint validateSortedKeys(
    T *resKey,
    T *srcKey,
    uint batchSize,
    uint arrayLength,
    uint numValues,
    uint sortDir
)
{

    if (arrayLength < 2)
    {
        printf("validateSortedKeys(): arrays too short, exiting...\n");
        return 1;
    }

    printf("...inspecting keys array: ");
    int flag = 1;

    for (uint j = 0; j < batchSize; j++, srcKey += arrayLength, resKey += arrayLength)
    {
        //Finally check the ordering
        for (uint i = 0; i < arrayLength - 1; i++)
            if ((sortDir && (resKey[i] > resKey[i + 1])) || (!sortDir && (resKey[i] < resKey[i + 1])))
            {
                fprintf(stderr, "***Set %u result key array is not ordered properly***\n", j);
                flag = 0;
                goto brk;
            }
    }

brk:
    if (flag) printf("OK\n");

    return flag;
}

static inline __host__ __device__ uint iDivUp(uint a, uint b)
{
    return ((a % b) == 0) ? (a / b) : (a / b + 1);
}

static inline __host__ __device__ uint getSampleCount(uint dividend)
{
    return iDivUp(dividend, SAMPLE_STRIDE);
}

static inline __device__ uint nextPowerOfTwo(uint x)
{
    /*
        --x;
        x |= x >> 1;
        x |= x >> 2;
        x |= x >> 4;
        x |= x >> 8;
        x |= x >> 16;
        return ++x;
    */
    return 1U << ((sizeof(uint)*8) - __clz(x - 1));
}

template<uint sortDir, typename T> static inline __device__ uint binarySearchInclusive(T val, T *data, uint L, uint stride) 
{
    if (L == 0)
    {
        return 0;
    }

    uint pos = 0;

    for (; stride > 0; stride >>= 1)
    {
        uint newPos = umin(pos + stride, L);

        if ((sortDir && (data[newPos - 1] <= val)) || (!sortDir && (data[newPos - 1] >= val)))
        {
            pos = newPos;
        }
    }

    return pos;
}

template<uint sortDir,typename T> static inline __device__ uint binarySearchExclusive(T val, T *data, uint L, uint stride)
{
    if (L == 0)
    {
        return 0;
    }

    uint pos = 0;

    for (; stride > 0; stride >>= 1)
    {
        uint newPos = umin(pos + stride, L);

        if ((sortDir && (data[newPos - 1] < val)) || (!sortDir && (data[newPos - 1] > val)))
        {
            pos = newPos;
        }
    }

    return pos;
}

////////////////////////////////////////////////////////////////////////////////
// Bottom-level merge sort (binary search-based)
////////////////////////////////////////////////////////////////////////////////
template<uint sortDir, typename T> __global__ void mergeSortSharedKernel(
    T *d_DstKey,
    T *d_SrcKey,
    uint arrayLength
)
{
    // Handle to thread block group
    cooperative_groups::thread_block cta = cooperative_groups::this_thread_block();
    __shared__ T s_key[SHARED_SIZE_LIMIT];

    d_SrcKey += blockIdx.x * SHARED_SIZE_LIMIT + threadIdx.x;
    d_DstKey += blockIdx.x * SHARED_SIZE_LIMIT + threadIdx.x;
    s_key[threadIdx.x +                       0] = d_SrcKey[                      0];
    s_key[threadIdx.x + (SHARED_SIZE_LIMIT / 2)] = d_SrcKey[(SHARED_SIZE_LIMIT / 2)];

    for (uint stride = 1; stride < arrayLength; stride <<= 1)
    {
        uint     lPos = threadIdx.x & (stride - 1);
        T *baseKey = s_key + 2 * (threadIdx.x - lPos);

        cooperative_groups::sync(cta);
        T keyA = baseKey[lPos +      0];
        T keyB = baseKey[lPos + stride];
        uint posA = binarySearchExclusive<sortDir>(keyA, baseKey + stride, stride, stride) + lPos;
        uint posB = binarySearchInclusive<sortDir>(keyB, baseKey +      0, stride, stride) + lPos;

        cooperative_groups::sync(cta);
        baseKey[posA] = keyA;
        baseKey[posB] = keyB;
    }

    cooperative_groups::sync(cta);
    d_DstKey[                      0] = s_key[threadIdx.x +                       0];
    d_DstKey[(SHARED_SIZE_LIMIT / 2)] = s_key[threadIdx.x + (SHARED_SIZE_LIMIT / 2)];
}

template <typename T>
static void mergeSortShared(
    T *d_DstKey,
    T *d_SrcKey,
    uint batchSize,
    uint arrayLength,
    uint sortDir
)
{
    if (arrayLength < 2)
    {
        return;
    }

    assert(SHARED_SIZE_LIMIT % arrayLength == 0);
    assert(((batchSize * arrayLength) % SHARED_SIZE_LIMIT) == 0);
    uint  blockCount = batchSize * arrayLength / SHARED_SIZE_LIMIT;
    uint threadCount = SHARED_SIZE_LIMIT / 2;

    if (sortDir)
    {
        mergeSortSharedKernel<1U,T><<<blockCount, threadCount>>>(d_DstKey, d_SrcKey, arrayLength);
        // getLastCudaError("mergeSortShared<1><<<>>> failed\n");
    }
    else
    {
        mergeSortSharedKernel<0U,T><<<blockCount, threadCount>>>(d_DstKey, d_SrcKey, arrayLength);
        // getLastCudaError("mergeSortShared<0><<<>>> failed\n");
    }
}



////////////////////////////////////////////////////////////////////////////////
// Merge step 1: generate sample ranks
////////////////////////////////////////////////////////////////////////////////
template<uint sortDir, typename T> __global__ void generateSampleRanksKernel(
    uint *d_RanksA,
    uint *d_RanksB,
    T *d_SrcKey,
    uint stride,
    uint N,
    uint threadCount
)
{
    uint pos = blockIdx.x * blockDim.x + threadIdx.x;

    if (pos >= threadCount)
    {
        return;
    }

    const uint           i = pos & ((stride / SAMPLE_STRIDE) - 1);
    const uint segmentBase = (pos - i) * (2 * SAMPLE_STRIDE);
    d_SrcKey += segmentBase;
    d_RanksA += segmentBase / SAMPLE_STRIDE;
    d_RanksB += segmentBase / SAMPLE_STRIDE;

    const uint segmentElementsA = stride;
    const uint segmentElementsB = umin(stride, N - segmentBase - stride);
    const uint  segmentSamplesA = getSampleCount(segmentElementsA);
    const uint  segmentSamplesB = getSampleCount(segmentElementsB);

    if (i < segmentSamplesA)
    {
        d_RanksA[i] = i * SAMPLE_STRIDE;
        d_RanksB[i] = binarySearchExclusive<sortDir>(
                          d_SrcKey[i * SAMPLE_STRIDE], d_SrcKey + stride,
                          segmentElementsB, nextPowerOfTwo(segmentElementsB)
                      );
    }

    if (i < segmentSamplesB)
    {
        d_RanksB[(stride / SAMPLE_STRIDE) + i] = i * SAMPLE_STRIDE;
        d_RanksA[(stride / SAMPLE_STRIDE) + i] = binarySearchInclusive<sortDir>(
                                                     d_SrcKey[stride + i * SAMPLE_STRIDE], d_SrcKey + 0,
                                                     segmentElementsA, nextPowerOfTwo(segmentElementsA)
                                                 );
    }
}

template <typename T>
static void generateSampleRanks(
    uint *d_RanksA,
    uint *d_RanksB,
    T *d_SrcKey,
    uint stride,
    uint N,
    uint sortDir
)
{
    uint lastSegmentElements = N % (2 * stride);
    uint         threadCount = (lastSegmentElements > stride) ? (N + 2 * stride - lastSegmentElements) / (2 * SAMPLE_STRIDE) : (N - lastSegmentElements) / (2 * SAMPLE_STRIDE);

    if (sortDir)
    {
        generateSampleRanksKernel<1U><<<iDivUp(threadCount, 256), 256>>>(d_RanksA, d_RanksB, d_SrcKey, stride, N, threadCount);
        // getLastCudaError("generateSampleRanksKernel<1U><<<>>> failed\n");
    }
    else
    {
        generateSampleRanksKernel<0U><<<iDivUp(threadCount, 256), 256>>>(d_RanksA, d_RanksB, d_SrcKey, stride, N, threadCount);
        // getLastCudaError("generateSampleRanksKernel<0U><<<>>> failed\n");
    }
}



////////////////////////////////////////////////////////////////////////////////
// Merge step 2: generate sample ranks and indices
////////////////////////////////////////////////////////////////////////////////
__global__ void mergeRanksAndIndicesKernel(
    uint *d_Limits,
    uint *d_Ranks,
    uint stride,
    uint N,
    uint threadCount
)
{
    uint pos = blockIdx.x * blockDim.x + threadIdx.x;

    if (pos >= threadCount)
    {
        return;
    }

    const uint           i = pos & ((stride / SAMPLE_STRIDE) - 1);
    const uint segmentBase = (pos - i) * (2 * SAMPLE_STRIDE);
    d_Ranks  += (pos - i) * 2;
    d_Limits += (pos - i) * 2;

    const uint segmentElementsA = stride;
    const uint segmentElementsB = umin(stride, N - segmentBase - stride);
    const uint  segmentSamplesA = getSampleCount(segmentElementsA);
    const uint  segmentSamplesB = getSampleCount(segmentElementsB);

    if (i < segmentSamplesA)
    {
        uint dstPos = binarySearchExclusive<1U>(d_Ranks[i], d_Ranks + segmentSamplesA, segmentSamplesB, nextPowerOfTwo(segmentSamplesB)) + i;
        d_Limits[dstPos] = d_Ranks[i];
    }

    if (i < segmentSamplesB)
    {
        uint dstPos = binarySearchInclusive<1U>(d_Ranks[segmentSamplesA + i], d_Ranks, segmentSamplesA, nextPowerOfTwo(segmentSamplesA)) + i;
        d_Limits[dstPos] = d_Ranks[segmentSamplesA + i];
    }
}

static void mergeRanksAndIndices(
    uint *d_LimitsA,
    uint *d_LimitsB,
    uint *d_RanksA, 
    uint *d_RanksB,
    uint stride,
    uint N
)
{
    uint lastSegmentElements = N % (2 * stride);
    uint         threadCount = (lastSegmentElements > stride) ? (N + 2 * stride - lastSegmentElements) / (2 * SAMPLE_STRIDE) : (N - lastSegmentElements) / (2 * SAMPLE_STRIDE);

    mergeRanksAndIndicesKernel<<<iDivUp(threadCount, 256), 256>>>(
        d_LimitsA,
        d_RanksA,
        stride,
        N,
        threadCount
    );
    // getLastCudaError("mergeRanksAndIndicesKernel(A)<<<>>> failed\n");

    mergeRanksAndIndicesKernel<<<iDivUp(threadCount, 256), 256>>>(
        d_LimitsB,
        d_RanksB,
        stride,
        N,
        threadCount
    );
    // getLastCudaError("mergeRanksAndIndicesKernel(B)<<<>>> failed\n");
}



////////////////////////////////////////////////////////////////////////////////
// Merge step 3: merge elementary intervals
////////////////////////////////////////////////////////////////////////////////
template<uint sortDir, typename T> inline __device__ void merge(
    T *dstKey,
    T *srcAKey,
    T *srcBKey,
    uint lenA,
    uint nPowTwoLenA,
    uint lenB,
    uint nPowTwoLenB,
    cooperative_groups::thread_block cta
)
{
    T keyA, keyB;
    uint dstPosA, dstPosB;

    if (threadIdx.x < lenA)
    {
        keyA = srcAKey[threadIdx.x];
        dstPosA = binarySearchExclusive<sortDir>(keyA, srcBKey, lenB, nPowTwoLenB) + threadIdx.x;
    }

    if (threadIdx.x < lenB)
    {
        keyB = srcBKey[threadIdx.x];
        dstPosB = binarySearchInclusive<sortDir>(keyB, srcAKey, lenA, nPowTwoLenA) + threadIdx.x;
    }

    cooperative_groups::sync(cta);

    if (threadIdx.x < lenA)
    {
        dstKey[dstPosA] = keyA;
    }

    if (threadIdx.x < lenB)
    {
        dstKey[dstPosB] = keyB;
    }
}

template<uint sortDir, typename T> __global__ void mergeElementaryIntervalsKernel(
    T *d_DstKey,
    T *d_SrcKey,
    uint *d_LimitsA,
    uint *d_LimitsB,
    uint stride,
    uint N
)
{
    // Handle to thread block group
    cooperative_groups::thread_block cta = cooperative_groups::this_thread_block();
    __shared__ T s_key[2 * SAMPLE_STRIDE];

    const uint   intervalI = blockIdx.x & ((2 * stride) / SAMPLE_STRIDE - 1);
    const uint segmentBase = (blockIdx.x - intervalI) * SAMPLE_STRIDE;
    d_SrcKey += segmentBase;
    d_DstKey += segmentBase;

    //Set up threadblock-wide parameters
    __shared__ uint startSrcA, startSrcB, lenSrcA, lenSrcB, startDstA, startDstB;

    if (threadIdx.x == 0)
    {
        uint segmentElementsA = stride;
        uint segmentElementsB = umin(stride, N - segmentBase - stride);
        uint  segmentSamplesA = getSampleCount(segmentElementsA);
        uint  segmentSamplesB = getSampleCount(segmentElementsB);
        uint   segmentSamples = segmentSamplesA + segmentSamplesB;

        startSrcA    = d_LimitsA[blockIdx.x];
        startSrcB    = d_LimitsB[blockIdx.x];
        uint endSrcA = (intervalI + 1 < segmentSamples) ? d_LimitsA[blockIdx.x + 1] : segmentElementsA;
        uint endSrcB = (intervalI + 1 < segmentSamples) ? d_LimitsB[blockIdx.x + 1] : segmentElementsB;
        lenSrcA      = endSrcA - startSrcA;
        lenSrcB      = endSrcB - startSrcB;
        startDstA    = startSrcA + startSrcB;
        startDstB    = startDstA + lenSrcA;
    }

    //Load main input data
    cooperative_groups::sync(cta);

    if (threadIdx.x < lenSrcA)
    {
        s_key[threadIdx.x +             0] = d_SrcKey[0 + startSrcA + threadIdx.x];
    }

    if (threadIdx.x < lenSrcB)
    {
        s_key[threadIdx.x + SAMPLE_STRIDE] = d_SrcKey[stride + startSrcB + threadIdx.x];
    }

    //Merge data in shared memory
    cooperative_groups::sync(cta);
    merge<sortDir>(
        s_key,
        s_key + 0,
        s_key + SAMPLE_STRIDE,
        lenSrcA, SAMPLE_STRIDE,
        lenSrcB, SAMPLE_STRIDE, 
        cta
    );

    //Store merged data
    cooperative_groups::sync(cta);

    if (threadIdx.x < lenSrcA)
    {
        d_DstKey[startDstA + threadIdx.x] = s_key[threadIdx.x];
    }

    if (threadIdx.x < lenSrcB)
    {
        d_DstKey[startDstB + threadIdx.x] = s_key[lenSrcA + threadIdx.x];
    }
}

template <typename T>
static void mergeElementaryIntervals(
    T *d_DstKey,
    T *d_SrcKey,
    uint *d_LimitsA,
    uint *d_LimitsB,
    uint stride,
    uint N,
    uint sortDir
)
{
    uint lastSegmentElements = N % (2 * stride);
    uint          mergePairs = (lastSegmentElements > stride) ? getSampleCount(N) : (N - lastSegmentElements) / SAMPLE_STRIDE;

    if (sortDir)
    {
        mergeElementaryIntervalsKernel<1U><<<mergePairs, SAMPLE_STRIDE>>>(
            d_DstKey,
            d_SrcKey,
            d_LimitsA,
            d_LimitsB,
            stride,
            N
        );
        // getLastCudaError("mergeElementaryIntervalsKernel<1> failed\n");
    }
    else
    {
        mergeElementaryIntervalsKernel<0U><<<mergePairs, SAMPLE_STRIDE>>>(
            d_DstKey,
            d_SrcKey,
            d_LimitsA,
            d_LimitsB,
            stride,
            N
        );
        // getLastCudaError("mergeElementaryIntervalsKernel<0> failed\n");
    }
}


template <typename T>
 inline __device__ void Comparator(
     T &keyA,
     T &valA,
     T &keyB,
     T &valB,
     uint arrowDir
 )
 {
     T t;
 
     if ((keyA > keyB) == arrowDir)
     {
         t = keyA;
         keyA = keyB;
         keyB = t;
         t = valA;
         valA = valB;
         valB = t;
     }
 }
 
 template <typename T>
 __global__ void bitonicSortSharedKernel(
     T *d_DstKey,
     T *d_DstVal,
     T *d_SrcKey,
     T *d_SrcVal,
     uint arrayLength,
     uint sortDir
 )
 {
     // Handle to thread block group
     cooperative_groups::thread_block cta = cooperative_groups::this_thread_block();
     //Shared memory storage for one or more short vectors
     __shared__ T s_key[SHARED_SIZE_LIMIT];
     __shared__ T s_val[SHARED_SIZE_LIMIT];
 
     //Offset to the beginning of subbatch and load data
     d_SrcKey += blockIdx.x * SHARED_SIZE_LIMIT + threadIdx.x;
     d_SrcVal += blockIdx.x * SHARED_SIZE_LIMIT + threadIdx.x;
     d_DstKey += blockIdx.x * SHARED_SIZE_LIMIT + threadIdx.x;
     d_DstVal += blockIdx.x * SHARED_SIZE_LIMIT + threadIdx.x;
     s_key[threadIdx.x +                       0] = d_SrcKey[                      0];
     s_val[threadIdx.x +                       0] = d_SrcVal[                      0];
     s_key[threadIdx.x + (SHARED_SIZE_LIMIT / 2)] = d_SrcKey[(SHARED_SIZE_LIMIT / 2)];
     s_val[threadIdx.x + (SHARED_SIZE_LIMIT / 2)] = d_SrcVal[(SHARED_SIZE_LIMIT / 2)];
 
     for (uint size = 2; size < arrayLength; size <<= 1)
     {
         //Bitonic merge
         uint dir = (threadIdx.x & (size / 2)) != 0;
 
         for (uint stride = size / 2; stride > 0; stride >>= 1)
         {
             cooperative_groups::sync(cta);
             uint pos = 2 * threadIdx.x - (threadIdx.x & (stride - 1));
             Comparator(
                 s_key[pos +      0], s_val[pos +      0],
                 s_key[pos + stride], s_val[pos + stride],
                 dir
             );
         }
     }
 
     //ddd == sortDir for the last bitonic merge step
     {
         for (uint stride = arrayLength / 2; stride > 0; stride >>= 1)
         {
             cooperative_groups::sync(cta);
             uint pos = 2 * threadIdx.x - (threadIdx.x & (stride - 1));
             Comparator(
                 s_key[pos +      0], s_val[pos +      0],
                 s_key[pos + stride], s_val[pos + stride],
                 sortDir
             );
         }
     }
 
     cooperative_groups::sync(cta);
     d_DstKey[                      0] = s_key[threadIdx.x +                       0];
     d_DstVal[                      0] = s_val[threadIdx.x +                       0];
     d_DstKey[(SHARED_SIZE_LIMIT / 2)] = s_key[threadIdx.x + (SHARED_SIZE_LIMIT / 2)];
     d_DstVal[(SHARED_SIZE_LIMIT / 2)] = s_val[threadIdx.x + (SHARED_SIZE_LIMIT / 2)];
 }
 
 //Helper function (also used by odd-even merge sort)
 uint factorRadix2(uint *log2L, uint L)
 {
     if (!L)
     {
         *log2L = 0;
         return 0;
     }
     else
     {
         for (*log2L = 0; (L & 1) == 0; L >>= 1, *log2L++);
 
         return L;
     }
 }
 
 template <typename T>
 void bitonicSortShared(
     T *d_DstKey,
     T *d_DstVal,
     T *d_SrcKey,
     T *d_SrcVal,
     uint batchSize,
     uint arrayLength,
     uint sortDir
 )
 {
     //Nothing to sort
     if (arrayLength < 2)
     {
         return;
     }
 
     //Only power-of-two array lengths are supported by this implementation
     uint log2L;
     uint factorizationRemainder = factorRadix2(&log2L, arrayLength);
     assert(factorizationRemainder == 1);
 
     uint  blockCount = batchSize * arrayLength / SHARED_SIZE_LIMIT;
     uint threadCount = SHARED_SIZE_LIMIT / 2;
 
     assert(arrayLength <= SHARED_SIZE_LIMIT);
     assert((batchSize * arrayLength) % SHARED_SIZE_LIMIT == 0);
 
     bitonicSortSharedKernel<<<blockCount, threadCount>>>(d_DstKey, d_DstVal, d_SrcKey, d_SrcVal, arrayLength, sortDir);
    //  getLastCudaError("bitonicSortSharedKernel<<<>>> failed!\n");
 }
 
 
 
 ////////////////////////////////////////////////////////////////////////////////
 // Merge step 3: merge elementary intervals
 ////////////////////////////////////////////////////////////////////////////////

 
 template<uint sortDir,typename T> static inline __device__ void ComparatorExtended(
     T &keyA,
     T &valA,
     bool &flagA,
     T &keyB,
     T &valB,
     bool &flagB,
     uint arrowDir
 )
 {
     if (
         (!(flagA || flagB) && ((keyA > keyB) == arrowDir)) ||
         ((arrowDir == sortDir) && (flagA == 1)) ||
         ((arrowDir != sortDir) && (flagB == 1))
     )
     {
         T t;
         t = keyA;
         keyA = keyB;
         keyB = t;
         t = valA;
         valA = valB;
         valB = t;
         bool t2;
         t2 = flagA;
         flagA = flagB;
         flagB = t2;
     }
 }
 
 template<uint sortDir,typename T> __global__ void bitonicMergeElementaryIntervalsKernel(
     T *d_DstKey,
     T *d_DstVal,
     T *d_SrcKey,
     T *d_SrcVal,
     uint *d_LimitsA,
     uint *d_LimitsB,
     uint stride,
     uint N
 )
 {
     // Handle to thread block group
     cooperative_groups::thread_block cta = cooperative_groups::this_thread_block();
     __shared__ T s_key[2 * SAMPLE_STRIDE];
     __shared__ T s_val[2 * SAMPLE_STRIDE];
     __shared__ bool s_inf[2 * SAMPLE_STRIDE];
 
     const uint   intervalI = blockIdx.x & ((2 * stride) / SAMPLE_STRIDE - 1);
     const uint segmentBase = (blockIdx.x - intervalI) * SAMPLE_STRIDE;
     d_SrcKey += segmentBase;
     d_SrcVal += segmentBase;
     d_DstKey += segmentBase;
     d_DstVal += segmentBase;
 
     //Set up threadblock-wide parameters
     __shared__ uint startSrcA, lenSrcA, startSrcB, lenSrcB, startDst;
 
     if (threadIdx.x == 0)
     {
         uint segmentElementsA = stride;
         uint segmentElementsB = umin(stride, N - segmentBase - stride);
         uint  segmentSamplesA = stride / SAMPLE_STRIDE;
         uint  segmentSamplesB = getSampleCount(segmentElementsB);
         uint   segmentSamples = segmentSamplesA + segmentSamplesB;
 
         startSrcA    = d_LimitsA[blockIdx.x];
         startSrcB    = d_LimitsB[blockIdx.x];
         startDst     = startSrcA + startSrcB;
 
         uint endSrcA = (intervalI + 1 < segmentSamples) ? d_LimitsA[blockIdx.x + 1] : segmentElementsA;
         uint endSrcB = (intervalI + 1 < segmentSamples) ? d_LimitsB[blockIdx.x + 1] : segmentElementsB;
         lenSrcA      = endSrcA - startSrcA;
         lenSrcB      = endSrcB - startSrcB;
     }
 
     s_inf[threadIdx.x +             0] = 1;
     s_inf[threadIdx.x + SAMPLE_STRIDE] = 1;
 
     //Load input data
     cooperative_groups::sync(cta);
 
     if (threadIdx.x < lenSrcA)
     {
         s_key[threadIdx.x] = d_SrcKey[0 + startSrcA + threadIdx.x];
         s_val[threadIdx.x] = d_SrcVal[0 + startSrcA + threadIdx.x];
         s_inf[threadIdx.x] = 0;
     }
 
     //Prepare for bitonic merge by inversing the ordering
     if (threadIdx.x < lenSrcB)
     {
         s_key[2 * SAMPLE_STRIDE - 1 - threadIdx.x] = d_SrcKey[stride + startSrcB + threadIdx.x];
         s_val[2 * SAMPLE_STRIDE - 1 - threadIdx.x] = d_SrcVal[stride + startSrcB + threadIdx.x];
         s_inf[2 * SAMPLE_STRIDE - 1 - threadIdx.x] = 0;
     }
 
     //"Extended" bitonic merge
     for (uint stride = SAMPLE_STRIDE; stride > 0; stride >>= 1)
     {
         cooperative_groups::sync(cta);
         uint pos = 2 * threadIdx.x - (threadIdx.x & (stride - 1));
         ComparatorExtended<sortDir,T>(
             s_key[pos +      0], s_val[pos +      0], s_inf[pos +      0],
             s_key[pos + stride], s_val[pos + stride], s_inf[pos + stride],
             sortDir
         );
     }
 
     //Store sorted data
     cooperative_groups::sync(cta);
     d_DstKey += startDst;
     d_DstVal += startDst;
 
     if (threadIdx.x < lenSrcA)
     {
         d_DstKey[threadIdx.x] = s_key[threadIdx.x];
         d_DstVal[threadIdx.x] = s_val[threadIdx.x];
     }
 
     if (threadIdx.x < lenSrcB)
     {
         d_DstKey[lenSrcA + threadIdx.x] = s_key[lenSrcA + threadIdx.x];
         d_DstVal[lenSrcA + threadIdx.x] = s_val[lenSrcA + threadIdx.x];
     }
 }
 
 template <typename T>
 void bitonicMergeElementaryIntervals(
     T *d_DstKey,
     T *d_DstVal,
     T *d_SrcKey,
     T *d_SrcVal,
     uint *d_LimitsA,
     uint *d_LimitsB,
     uint stride,
     uint N,
     uint sortDir
 )
 {
     uint lastSegmentElements = N % (2 * stride);
 
     uint mergePairs =
         (lastSegmentElements > stride) ?
         getSampleCount(N) :
         (N - lastSegmentElements) / SAMPLE_STRIDE;
 
     if (sortDir)
     {
         bitonicMergeElementaryIntervalsKernel<1U,T><<<mergePairs, SAMPLE_STRIDE>>>(
             d_DstKey,
             d_DstVal,
             d_SrcKey,
             d_SrcVal,
             d_LimitsA,
             d_LimitsB,
             stride,
             N
         );
        //  getLastCudaError("mergeElementaryIntervalsKernel<1> failed\n");
     }
     else
     {
         bitonicMergeElementaryIntervalsKernel<0U,T><<<mergePairs, SAMPLE_STRIDE>>>(
             d_DstKey,
             d_DstVal,
             d_SrcKey,
             d_SrcVal,
             d_LimitsA,
             d_LimitsB,
             stride,
             N
         );
        //  getLastCudaError("mergeElementaryIntervalsKernel<0> failed\n");
     }
 }
 

template <typename T>
void mergeSort(
    T *d_DstKey,
    T *d_BufKey,
    T *d_SrcKey,
    uint *d_RanksA,
    uint *d_RanksB,
    uint *d_LimitsA,
    uint *d_LimitsB,
    uint N,
    uint sortDir
)
{
    uint stageCount = 0;

    for (uint stride = SHARED_SIZE_LIMIT; stride < N; stride <<= 1, stageCount++);

    T *ikey, *okey;

    if (stageCount & 1)
    {
        ikey = d_BufKey;
        okey = d_DstKey;
    }
    else
    {
        ikey = d_DstKey;
        okey = d_BufKey;
    }

    //uint MAX_SAMPLE_COUNT = (N+SAMPLE_STRIDE-1)/SAMPLE_STRIDE;
    // assert(N <= (SAMPLE_STRIDE * MAX_SAMPLE_COUNT));
    assert(N % SHARED_SIZE_LIMIT == 0);
    mergeSortShared<T>(ikey, d_SrcKey, N / SHARED_SIZE_LIMIT, SHARED_SIZE_LIMIT, sortDir);

    for (uint stride = SHARED_SIZE_LIMIT; stride < N; stride <<= 1)
    {
        uint lastSegmentElements = N % (2 * stride);

        //Find sample ranks and prepare for limiters merge
        generateSampleRanks<T>(d_RanksA, d_RanksB, ikey, stride, N, sortDir);

        //Merge ranks and indices
        mergeRanksAndIndices(d_LimitsA, d_LimitsB, d_RanksA, d_RanksB, stride, N);

        //Merge elementary intervals
        mergeElementaryIntervals(okey, ikey, d_LimitsA, d_LimitsB, stride, N, sortDir);

        if (lastSegmentElements <= stride)
        {
            //Last merge segment consists of a single array which just needs to be passed through
            (cudaMemcpy(okey + (N - lastSegmentElements), ikey + (N - lastSegmentElements), lastSegmentElements * sizeof(T), cudaMemcpyDeviceToDevice));
        }

        T *t;
        t = ikey;
        ikey = okey;
        okey = t;
    }
}

int main(int argc, char **argv)
{
    float *h_SrcKey, *h_DstKey;
    float *d_SrcKey, *d_BufKey, *d_DstKey;
    uint *d_RanksA, *d_RanksB, *d_LimitsA, *d_LimitsB;
    // StopWatchInterface *hTimer = NULL;

    //const uint   N = 131072*1024;
    // const uint   N = 2048;
    uint N = (uint)atoi(argv[1]);
    N = 1<<N;
    const uint DIR = 1;
    const uint numValues = 65536;

    printf("%s Starting...\n\n", argv[0]);

    // int dev = findCudaDevice(argc, (const char **) argv);

    // if (dev == -1)
    // {
    //     return EXIT_FAILURE;
    // }

    printf("Allocating and initializing host arrays...\n\n");
    printf("N=%u\n",N);
    h_SrcKey = (float *)malloc(N * sizeof(float));
    h_DstKey = (float *)malloc(N * sizeof(float));

    srand(2009);

    for (uint i = 0; i < N; i++)
    {
        h_SrcKey[i] = rand() % numValues;
    }


    printf("Allocating and initializing CUDA arrays...\n\n");
    printf("Initializing GPU merge sort...\n");
    // initMergeSort();
    (cudaMalloc((void **)&d_DstKey, N * sizeof(float)));
    (cudaMalloc((void **)&d_BufKey, N * sizeof(float)));
    (cudaMalloc((void **)&d_SrcKey, N * sizeof(float)));
    uint MAX_SAMPLE_COUNT = (N+SAMPLE_STRIDE-1)/SAMPLE_STRIDE;
    

    (cudaMalloc((void**)&d_RanksA, MAX_SAMPLE_COUNT*sizeof(uint)));
    (cudaMalloc((void**)&d_RanksB, MAX_SAMPLE_COUNT*sizeof(uint)));
    (cudaMalloc((void**)&d_LimitsA, MAX_SAMPLE_COUNT*sizeof(uint)));
    (cudaMalloc((void**)&d_LimitsB, MAX_SAMPLE_COUNT*sizeof(uint)));

    (cudaMemcpy(d_SrcKey, h_SrcKey, N * sizeof(float), cudaMemcpyHostToDevice));

    printf("Running GPU merge sort...\n");
    (cudaDeviceSynchronize());
    mergeSort<float>(
        d_DstKey,
        d_BufKey,
        d_SrcKey,
        d_RanksA,
        d_RanksB,
        d_LimitsA,
        d_LimitsB,
        N,
        DIR
    );
    (cudaDeviceSynchronize());


    printf("Reading back GPU merge sort results...\n");
    (cudaMemcpy(h_DstKey, d_DstKey, N * sizeof(float), cudaMemcpyDeviceToHost));

    printf("printing output keys...\n");
    if (N<=65536)
        for (int i = 0; i < N; i++){
            printf("%.0f ",h_DstKey[i]);
        }
    printf("\n");

    printf("Validating the results ... \n");
    uint keysFlag = validateSortedKeys<float>(
        h_DstKey,
        h_SrcKey,
        1,
        N,
        numValues,
        DIR
    );

    printf("Shutting down...\n");
    // closeMergeSort();
    // sdkDeleteTimer(&hTimer);
    (cudaFree(d_SrcKey));
    (cudaFree(d_BufKey));
    (cudaFree(d_DstKey));
    (cudaFree(d_RanksA));
    (cudaFree(d_RanksB));
    (cudaFree(d_LimitsA));
    (cudaFree(d_LimitsB));

    free(h_DstKey);
    free(h_SrcKey);

    exit(0);
}
