import numpy as np
import mxnet as mx
import time

def binary(num):
    import struct
    return ''.join(bin(c).replace('0b', '').rjust(8, '0') for c in struct.pack('!f', num))

def float32_to_int32(num):
    b = binary(num)
    if b[0]=='0':
        return int(b,2)
    else:
        return -int(b[1:],2)

def verify_xpu(N,s_percent,sample_rate,xpu):
    if xpu == 'cpu':
        ctx = mx.context.cpu(0)
    elif xpu == 'gpu':
        ctx = mx.context.gpu(0)
    else:
        raise ValueError(xpu)

    import random
    # a = [random.randint(-N//2,N//2) for i in range(N)]
    a = [i-N/2 for i in range(N)]
    to_compress = mx.nd.array(a,ctx=ctx,dtype='float32')
    compressed = mx.nd.zeros(shape=(2*N+2),ctx=ctx,dtype='float32')
    decompressed = mx.nd.zeros(shape=(N),ctx=ctx,dtype='float32')
    
    mx.nd.contrib.dgc_thrust(
        data=to_compress,
        s_percent = s_percent,
        sample_rate = sample_rate,
        out = compressed
    )
    print("a[:32]: ")
    for i in range(32):
        print((a[i]),end=' ')
    print()
    b = to_compress.asnumpy()
    print("b[:32]: ")
    for i in range(32):
        print(b[i],end=' ')
    print()

    c = compressed.asnumpy()
    print("c[:32]: ")
    for i in range(32):
        print(float32_to_int32(c[i]),end=' ')
    print()
    import math
    sample_cnt = math.ceil(N * s_percent)
    # selected = float32_to_int32(c[-1])
    selected = float32_to_int32(c[2*sample_cnt])
    print("sample_cnt(from python):", sample_cnt)
    print("selected(from python):",selected)
    print("v[:32]")
    for i in range(32):
        print(c[selected+i],end=' ')
    print()

    print("="*40)
    mx.nd.contrib.dgcr_thrust(
        data=compressed,
        original_size = N,
        to_decompress_data_num=selected,
        is_add_to=0,
        out = decompressed
    )
    print("validating decompressed...")
    d = decompressed.asnumpy()
    cnt = 0
    for i in range(N):
        if d[i] != 0:
            cnt += 1
            if abs((d[i]-a[i])/d[i]) > 1e-5:
                print("i={}\ta[i]={}\td[i]={}\n".format(i,a[i],d[i]))
                assert(0)
    print("cnt:",cnt)
    print("dgcr_thrust validating pass!")


    # print('c[:32]',c[:32])

def benchmark_xpu(N,s_percent,sample_rate,xpu):
    import math
    if xpu == 'cpu':
        ctx = mx.context.cpu(0)
    elif xpu == 'gpu':
        ctx = mx.context.gpu(0)
    else:
        raise ValueError(xpu)
    import random


    a = [i - N/2  for i in range(N)]
    to_compress = mx.nd.array(a,ctx=ctx,dtype='float32')
    compressed = mx.nd.zeros(shape=(N),ctx=ctx,dtype='float32')
    decompressed = mx.nd.zeros(shape=(N),ctx=ctx,dtype='float32')
    
    compression_time = []
    decompression_time = []
    times = 11
    for i in range(times):
        to_compress = mx.nd.array(a,ctx=ctx,dtype='float32')
        import time
        # time.sleep(0.1)
        t1 = time.time()
        mx.nd.contrib.dgc_thrust(
            data=to_compress,
            s_percent = s_percent,
            sample_rate = sample_rate,
            out = compressed
        )
        compressed.wait_to_read()
        t2 = time.time()
        sample_cnt = math.ceil(N * s_percent)
        selected = float32_to_int32(compressed[2*sample_cnt].asnumpy()[0])
        # print("selected:",selected)
        mx.nd.contrib.dgcr_thrust(
            data=compressed,
            original_size = N,
            to_decompress_data_num=selected,
            is_add_to=0,
            out = decompressed
        )
        decompressed.wait_to_read()
        # mx.nd.contrib.zgcr(
        #     to_decompress = compressed,
        #     threshold = threshold,
        #     out = after_decompress
        # )
        # after_decompress.wait_to_read()
        t3 = time.time()
        if i>0:
            compression_time.append(t2-t1)
            decompression_time.append(t3-t2)
    return sum(compression_time)/len(compression_time),sum(decompression_time)/len(decompression_time)
    
def num_float_to_size(n):
    size_B = n*4
    if size_B < 1024:
        return '{}B'.format(size_B)
    size_KB = size_B/1024
    if size_KB < 1024:
        return '{}KB'.format(size_KB)
    size_MB = size_KB/1024
    if size_MB < 1024:
        return '{}MB'.format(size_MB)
    size_GB = size_MB/1024
    return '{}GB'.format(size_GB)
    





if __name__ == '__main__':
    # verify_xpu(2**16,0.01,0.01,'gpu')
    # verify_xpu(2**7,0.01,0.01,'gpu')
    # exit(0)

    output_csv = "dgc_thrust.csv"
    with open(output_csv,'w') as f:
        f.write("N,size,raio,compression_time,decompresstion_time\n")

    # for N_lg2 in range(27,5,-1):
    for N_lg2 in range(4,28):
        for ratio in [0.001,0.01]:
            N = 2**N_lg2
            # N=589824
            result = benchmark_xpu(N,ratio,0.001,'gpu')
            print("N:{}\traio:{}\tcompression_time:{}\tdecompression_time:{}".format(N,ratio,result[0],result[1]))
            with open(output_csv,'a') as f:
                f.write("{N},{ratio},{size},{ct},{dt}\n".format(
                    N=N, 
                    ratio=ratio,
                    size = num_float_to_size(N),
                    ct = result[0],
                    dt = result[1]
                ))

