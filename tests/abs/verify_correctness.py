import numpy as np
import mxnet as mx
import time

if __name__ == '__main__':
    a = [i for i in range(2**10)]
    a = np.array(a)
    print("a prepared!")
    b = mx.nd.array(a)
    print("b prepared!")
    t1 = time.time()
    b.reshape((2**9,2))
    #c = mx.nd.contrib.abs(data=[b,b])
    # c = mx.nd.contrib.abs(data=(b,b))
    # c = mx.nd.contrib.abs(data=[b,b])
    c = mx.nd.contrib.abs(data=b)
    c.wait_to_read()
    t2 = time.time()
    d = c.asnumpy()
    print("d[:20]",d[:20])
    print("cost time (s):",t2-t1)
    exit()


    a = [i-10 for i in range(20)]
    a = np.array(a)
    #a = np.array([0,1,2,3,4,5,6,7,8,9,10,11,12,12,12])
    print("origin data:",a)
    b = mx.nd.array(a)
    print("input data before abs:", b)
    c = mx.nd.contrib.abs(data=b)
    print("output data:",c)
    print("input data after abs:",b)



