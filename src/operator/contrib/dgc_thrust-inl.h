#ifndef MXNET_OPERATOR_CONTRIB_DGC_THRUST_INL_H_
#define MXNET_OPERATOR_CONTRIB_DGC_THRUST_INL_H_

#include <mxnet/operator_util.h>
#include <vector>
#include "../mshadow_op.h"
#include "../mxnet_op.h"
#include "../operator_common.h"
#include "../elemwise_op_common.h"
#include "../tensor/init_op.h"

#include <thrust/random.h>
// #include <thrust/device_vector.h>
#include <thrust/sort.h>  //sort()
#include <thrust/execution_policy.h>  //thrust::device
#include <thrust/functional.h>  //greater<float>
#include <thrust/copy.h>  //copy_if
#include <thrust/iterator/counting_iterator.h>  // counting_iterator
#include <thrust/transform.h> //trnasform

#include <chrono>
#include <iostream>
#include <fstream>
#include <unistd.h>
#include <sstream>
#include <limits.h>
#include <string>

#include "get_policy-inl.h"

#include "ZQ_CPP_LIB/time_cost.hpp"
// #define DEBUG


namespace mxnet {
namespace op {

struct DGC_THRUSTParam : public dmlc::Parameter<DGC_THRUSTParam> {
  double s_percent;
  double sample_rate;
  DMLC_DECLARE_PARAMETER(DGC_THRUSTParam){
    DMLC_DECLARE_FIELD(s_percent)
      .set_default(0.01)
      .describe("Range of values:(0,1], determines how many values should be sent out");
    DMLC_DECLARE_FIELD(sample_rate)
      .set_default(0.001)
      .describe("input.size * sample_rate = sample_cnt");
  };
};

inline bool DGC_THRUSTOpShape(const nnvm::NodeAttrs& attrs,
                             mxnet::ShapeVector* in_attrs,
                             mxnet::ShapeVector* out_attrs) {
  CHECK_EQ(in_attrs->size(), 1U);
  CHECK_EQ(out_attrs->size(), 1U);


  size_t output_size = in_attrs->at(0)[0];
  if (out_attrs->at(0).ndim()==0U){
    out_attrs->at(0) = in_attrs->at(0); // copy ndim
    out_attrs->at(0)[0] = output_size;
  }
  else{
    CHECK_GE(out_attrs->at(0)[0],output_size);
  };

  return true;
}

inline bool DGC_THRUSTOpType(const nnvm::NodeAttrs& attrs,
                            std::vector<int>* in_attrs,
                            std::vector<int>* out_attrs) {
  CHECK_EQ(in_attrs->size(), 1U);
  CHECK_EQ(out_attrs->size(), 1U);

  out_attrs->at(0) = 0; //KFloat32=0, could transfer to int by (uint32_t)
  return in_attrs->at(0) == 0; //original data, should be float 32 type
}



struct DGC_SAMPLING_FORWARD{
  MSHADOW_XINLINE static void Map(
    int i,
    float* in_float,
    float* out_float,
    size_t input_size,
    unsigned long long t
  ){
    thrust::default_random_engine rng;
    thrust::uniform_int_distribution<uint32_t> dist(0,input_size-1);
    rng.discard(t+i);
    out_float[i] = std::abs(in_float[dist(rng)]);
    // ((uint32_t*)out_float)[i] = (((uint32_t*)in_float)[idx] << 1) >> 1;
  }
};
struct sampling{
  float* in_float;
  size_t input_size;
  unsigned long long t;
  sampling(
    float* _in_float,
    size_t _input_size,
    unsigned long long _t
  ):
    in_float(_in_float),
    input_size(_input_size),
    t(_t)
  {}
  __host__ __device__
  float operator()(const int32_t&i) const{
    thrust::default_random_engine rng;
    thrust::uniform_int_distribution<uint32_t> dist(0,input_size-1);
    rng.discard(t+i);
    return std::abs(in_float[dist(rng)]);
  }
};

struct greater_or_equal{
  const float threshold;
  greater_or_equal(float t): threshold(t){}

  __host__ __device__
  bool operator()(const float&x) const {
    return (x>=threshold) || (x<=-threshold);
  }
};
struct greater{
  const float threshold;
  greater(float t): threshold(t){}

  __host__ __device__
  bool operator()(const float&x) const {
    return (x>threshold) || (x<-threshold);
  }
};


struct minus_threshold{
  const float threshold;
  minus_threshold(float t): threshold(t){}

  __host__ __device__
  float operator()(const float&x){
    if (x>=threshold){
      return x-threshold;
    }
    else if (x<=-threshold){
      return x+threshold;
    }
    else{
      return x;
    }
  }
};

struct index_to_data{
  float* in_float;
  index_to_data(float* in_float_): in_float(in_float_){}

  __host__ __device__
  float operator()(const int32_t&i){
    float t = in_float[i];
    in_float[i] = 0;
    return t;
  }
};

struct decompress_write_to{
  int32_t* in_int32_t;
  float *in_float, *out_float;
  int32_t to_decompress_data_num;
  decompress_write_to(
    int32_t* a,
    float* b,
    float* c,
    int32_t d
  ){
    in_int32_t = a;
    in_float = b;
    out_float = c;
    to_decompress_data_num = d;
  }
  __host__ __device__
  int32_t operator()(const int32_t& i){
    out_float[in_int32_t[i]] = in_float[i + to_decompress_data_num];
    return i;
  }
};
struct decompress_add_to{
  int32_t* in_int32_t;
  float *in_float, *out_float;
  int32_t to_decompress_data_num;
  decompress_add_to(
    int32_t* a,
    float* b,
    float* c,
    int32_t d
  ){
    in_int32_t = a;
    in_float = b;
    out_float = c;
    to_decompress_data_num = d;
  }
  __host__ __device__
  int32_t operator()(const int32_t& i){
    out_float[in_int32_t[i]] += in_float[i + to_decompress_data_num];
    return i;
  }
};

struct cmp_float_data_by_int32_index{
  const float* in_float;
  cmp_float_data_by_int32_index(float* in_float_) : in_float(in_float_){}

  __host__ __device__
  bool operator()(const int32_t&i, const int32_t& j){
    return std::abs(in_float[i]) > std::abs(in_float[j]);
  }
};

#define likely(x) __builtin_expect(!!(x), 1)
#define unlikely(x) __builtin_expect(!!(x), 0)

template<typename xpu, typename policy_t>
void DGC_THRUSTOpForward(const nnvm::NodeAttrs& attrs,
                        const OpContext& ctx,
                        const std::vector<TBlob>& inputs,
                        const std::vector<OpReqType>& req,
                        const std::vector<TBlob>& outputs) {
  // printf("DGC_THRUSTOpForward start!\n");
  // zq_cpp_lib::time_cost zt;
  // zt.start();
  policy_t policy = get_policy<policy_t>::get(ctx);
  CHECK_EQ(req.size(), 1U);
  const DGC_THRUSTParam& param = nnvm::get<DGC_THRUSTParam>(attrs.parsed);
  const TBlob& in_data = inputs[0];
  const TBlob& out_data = outputs[0];

  auto in_float = to_array(in_data,float,float);
  auto out_float = to_array(out_data,float,float);
  auto out_int32_t = to_array(out_data,int32_t,float);

  using namespace mxnet_op;
  
  // ===========smapling===========
  //for in_data.Size()*param.sample_rate = 0: make it = 1
  // std::ceil(1000(int32_t) * 0.001(float)) = 2
  size_t sample_cnt = static_cast<size_t>(std::ceil(in_data.Size()*param.sample_rate));
  size_t expected_selected = static_cast<size_t>(std::ceil(in_data.Size()*param.s_percent));
  thrust::counting_iterator<int32_t> index_sequence_begin(0);
  // zt.record("initialize");
  thrust::transform(
    policy,
    index_sequence_begin,
    index_sequence_begin+sample_cnt,
    out_float,
    sampling(
      in_float,
      in_data.Size(),
      static_cast<unsigned long long>(
        std::chrono::high_resolution_clock::now()
        .time_since_epoch()
        .count()
      )
    )
  );
  // zt.record("sampling");


  // ===============sorting to get threshold=============
  thrust::sort(
    policy,
    out_float,
    out_float+sample_cnt,
    thrust::greater<float>()
  );
  // zt.record("sort sampling datas");
  float threshold;
  get_policy<policy_t>::memcpyOut(
    &threshold,
    out_float+static_cast<int>(sample_cnt * param.s_percent), // can not call ceil() here
    sizeof(float),
    ctx
  );
  get_policy<policy_t>::streamSynchronize(ctx);
  // zt.record("memcpyOut threshold");

  //================filter out indices =================
  auto out_int32_t_end = thrust::copy_if(
    policy,
    index_sequence_begin,
    index_sequence_begin + in_data.Size(),    
    in_float,
    out_int32_t,
    greater(threshold)  
  );
  // zt.record("copy_if index");
  // ======if more than half, sort to drop minor ones ==========
  size_t selected_num = out_int32_t_end - out_int32_t;
  // printf("selected_num=%d\n",selected_num);
  if (selected_num > expected_selected){
    thrust::sort(
      policy,
      out_int32_t,
      out_int32_t_end,
      cmp_float_data_by_int32_index(in_float)
    );
    selected_num = expected_selected;
  }
  // zt.record("sort index");

  get_policy<policy_t>::memcpyIn(
    out_float + (2*expected_selected),
    &selected_num,
    sizeof(float),
    ctx
  );
  // if (unlikely(sample_cnt==0)){
  if (unlikely(selected_num==0)){
    return ;
  }
  // zt.record("memcpyIn header");


  // ============copy values corrsponding to indices=============
  thrust::transform(
    policy,
    out_int32_t,
    out_int32_t + selected_num,
    out_float + selected_num,
    index_to_data(in_float) // if minus in_float here?
  );
  // zt.record("generate value");
  // printf("over. to call print_by_us\n");
  // zt.print_by_us();

}

struct DGCR_THRUSTParam : public dmlc::Parameter<DGCR_THRUSTParam> {
  int32_t original_size;
  int32_t to_decompress_data_num;
  int is_add_to;
  DMLC_DECLARE_PARAMETER(DGCR_THRUSTParam){
    DMLC_DECLARE_FIELD(original_size)
      .describe("The size of data before sparsity.");
    DMLC_DECLARE_FIELD(to_decompress_data_num)
      .set_default(-1)
      .describe("Indicates how many values compressed to decompress(last of quantized data may be uselesee). if not set, decompreess all.");
    DMLC_DECLARE_FIELD(is_add_to)
      .set_default(1)
      .describe("1: add_to; 0:write_to");
  };
};

inline bool DGCR_THRUSTOpShape(const nnvm::NodeAttrs& attrs,
                             mxnet::ShapeVector* in_attrs,
                             mxnet::ShapeVector* out_attrs) {
  CHECK_EQ(in_attrs->size(), 1U);
  CHECK_EQ(out_attrs->size(), 1U);

  const DGCR_THRUSTParam& param = nnvm::get<DGCR_THRUSTParam>(attrs.parsed);
  size_t output_size = param.original_size;
  if (out_attrs->at(0).ndim()==0U){
    out_attrs->at(0) = in_attrs->at(0); // copy ndim
    out_attrs->at(0)[0] = output_size;
  }
  else{
    CHECK_EQ(out_attrs->at(0)[0],output_size)
      << "Expected output size from param.original_size: "
      << output_size
      ;
  };

  return true;
}
inline bool DGCR_THRUSTOpType(const nnvm::NodeAttrs& attrs,
                            std::vector<int>* in_attrs,
                            std::vector<int>* out_attrs) {
  CHECK_EQ(in_attrs->size(), 1U);
  CHECK_EQ(out_attrs->size(), 1U);

  out_attrs->at(0) = 0; //KFloat32=0, original data should be float 32 type.
  return in_attrs->at(0) == 0; //sparsity is set as float32 data.
}



struct DGCR_WRITE_TO_FORWARD{
    MSHADOW_XINLINE static void Map(
        int i,
        float* in_float,
        int32_t* in_int32_t,
        float* out_float,
        int32_t to_decompress_data_num
    ){
        out_float[in_int32_t[i]] = in_float[i+to_decompress_data_num];
    }
};

struct DGCR_ADD_TO_FORWARD{
    MSHADOW_XINLINE static void Map(
        int i,
        float* in_float,
        int32_t* in_int32_t,
        float* out_float,
        int32_t to_decompress_data_num
    ){
        out_float[in_int32_t[i]] += in_float[i+to_decompress_data_num];
    }
};

template<typename xpu,typename policy_t>
void DGCR_THRUSTOpForward(const nnvm::NodeAttrs& attrs,
                        const OpContext& ctx,
                        const std::vector<TBlob>& inputs,
                        const std::vector<OpReqType>& req,
                        const std::vector<TBlob>& outputs) {
#ifdef DEBUG
  char hostname[HOST_NAME_MAX + 1];
  gethostname(hostname, HOST_NAME_MAX + 1);
  std::stringstream temp;
  std::string name;
  temp << hostname;
  temp >> name;

  static std::ofstream f_out("/home/gpu/mxnet/horovod/horovod-mxnet/dgc_" + name + "_forward.txt", std::ios::out | std::ios::app);
  auto start_time = std::chrono::high_resolution_clock::now();
#endif
  policy_t policy = get_policy<policy_t>::get(ctx);
  CHECK_EQ(req.size(), 1U);
  using namespace mxnet_op;
  const TBlob& in_data = inputs[0];
  const TBlob& out_data = outputs[0];

  auto in_float = to_array(in_data,float,float);
  auto in_int32_t = to_array(in_data,int32_t,float);
  auto out_float = to_array(out_data,float,float);
  const DGCR_THRUSTParam& param = nnvm::get<DGCR_THRUSTParam>(attrs.parsed);

  int32_t to_decompress_data_num = param.to_decompress_data_num;
  if (to_decompress_data_num < 0){
    // to_decompress_data_num = in_data.Size() >> 1;
    get_policy<policy_t>::memcpyOut(
      &to_decompress_data_num,
      in_int32_t+(in_data.Size()-1),
      sizeof(int32_t),
      ctx
    );
    get_policy<policy_t>::streamSynchronize(ctx);
  }

  thrust::counting_iterator<int32_t> index_sequence_begin(0);
  if (!param.is_add_to){
    thrust::transform(
      policy,
      index_sequence_begin,
      index_sequence_begin + to_decompress_data_num,
      in_int32_t,
      decompress_write_to(in_int32_t,in_float,out_float,to_decompress_data_num)
    );
  }
  else{
    thrust::transform(
      policy,
      index_sequence_begin,
      index_sequence_begin + to_decompress_data_num,
      in_int32_t,
      decompress_add_to(in_int32_t,in_float,out_float,to_decompress_data_num)
    );
  }
  // cudaDeviceSynchronize();
#ifdef DEBUG
  auto end_time = std::chrono::high_resolution_clock::now();
  if (f_out.is_open()) {
    std::string output = "dgcr input size: " + std::to_string(in_data.Size()) + "," + "output size: " + std::to_string(out_data.Size()) + ",";
    auto elapse = std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time).count();
    output += "elapse: " + std::to_string(elapse) + "\n";
    f_out << output;
    f_out.flush();
  }
#endif
}

}  // namespace op
}  // namespace mxnet

#endif  // MXNET_OPERATOR_CONTRIB_DGC_OP_INL_H_
