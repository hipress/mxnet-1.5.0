/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/*!
 * \file quad_function-inl.h
 * \brief Operator implementing quadratic function.
 * For using as an example in the tutorial of adding operators
 * in MXNet backend.
 */
#ifndef MXNET_OPERATOR_CONTRIB_OTG_OP_INL_H_
#define MXNET_OPERATOR_CONTRIB_OTG_OP_INL_H_

#include <mxnet/operator_util.h>
#include <vector>
#include "../mshadow_op.h"
#include "../mxnet_op.h"
#include "../operator_common.h"
#include "../elemwise_op_common.h"
#include "../tensor/init_op.h"

#include <chrono>
#include <iostream>
#include <chrono>
#include <string>
#include <fstream>

#include <thrust/random.h>
#include <thrust/sort.h>                       //sort()
#include <thrust/execution_policy.h>           //thrust::device
#include <thrust/functional.h>                 //greater<float>
#include <thrust/copy.h>                       //copy_if
#include <thrust/iterator/counting_iterator.h> // counting_iterator
#include <thrust/transform.h>                  //trnasform
#include <thrust/extrema.h>                    //minmax_elemetn
#include <thrust/system/cuda/execution_policy.h>
#include <thrust/system/omp/execution_policy.h> //
// #include <thrust/system/cpp/execution_policy.h>

#include "ZQ_CPP_LIB/likely.h"
#include "original_terngrad.h"

namespace mxnet
{
namespace op
{

struct OTGParam : public dmlc::Parameter<OTGParam>
{
  int bitwidth;
  int random;
  DMLC_DECLARE_PARAMETER(OTGParam)
  {
    DMLC_DECLARE_FIELD(bitwidth)
        .set_default(2)
        .describe("Number of bits used to quantize original value with type float32 as default. Its value should be  1, 2, 4 or 8");
    DMLC_DECLARE_FIELD(random)
        .set_default(0)
        .describe("Determine whether add random factor.");
  };
};

struct OTGRParam : public dmlc::Parameter<OTGRParam>
{
  int bitwidth;
  int tail;
  int is_add_to;
  DMLC_DECLARE_PARAMETER(OTGRParam)
  {
    DMLC_DECLARE_FIELD(bitwidth)
        .set_default(2)
        .describe("Number of bits used to quantize original value with type float32 as default. Its value should be  1, 2, 4 or 8");
    DMLC_DECLARE_FIELD(tail)
        .set_default(0)
        .describe("Number of tail space at last byte in quantized data");
    DMLC_DECLARE_FIELD(is_add_to)
        .set_default(0)
        .describe("0: write to; others: add to");
  };
};

inline bool OTGOpShape(const nnvm::NodeAttrs &attrs,
                            mxnet::ShapeVector *in_attrs,
                            mxnet::ShapeVector *out_attrs)
{
  CHECK_EQ(in_attrs->size(), 1U);
  CHECK_EQ(out_attrs->size(), 1U);

  const OTGParam &param = nnvm::get<OTGParam>(attrs.parsed);
  size_t data_per_byte = 8 / param.bitwidth;
  size_t segment_size = (in_attrs->at(0)[0] + data_per_byte - 1) / data_per_byte;
  size_t output_size = 10 + segment_size;

  //SHAPE_ASSIGN_CHECK(*out_attrs, 0, in_attrs->at(0));
  if (out_attrs->at(0).ndim() == 0U)
  {                                     // out not provided
    out_attrs->at(0) = in_attrs->at(0); // copy ndim
    out_attrs->at(0)[0] = output_size;
  }
  else
  {
    CHECK_GE(out_attrs->at(0)[0], output_size);
  };
  return out_attrs->at(0).ndim() != 0U && out_attrs->at(0).Size() != 0U;
}

inline bool OTGROpShape(const nnvm::NodeAttrs &attrs,
                             mxnet::ShapeVector *in_attrs,
                             mxnet::ShapeVector *out_attrs)
{
  CHECK_EQ(in_attrs->size(), 1U);
  CHECK_EQ(out_attrs->size(), 1U);
  const OTGRParam &param = nnvm::get<OTGRParam>(attrs.parsed);
  size_t data_per_byte = 8 / param.bitwidth;
  size_t output_size = (in_attrs->at(0)[0] - 10) * data_per_byte - param.tail;
  //SHAPE_ASSIGN_CHECK(*out_attrs, 0, in_attrs->at(0));
  if (out_attrs->at(0).ndim() == 0U)
  {
    out_attrs->at(0) = in_attrs->at(0);
    out_attrs->at(0)[0] = output_size;
  }
  else
  {
    CHECK_GE(out_attrs->at(0)[0], output_size);
  };
  return out_attrs->at(0).ndim() != 0U && out_attrs->at(0).Size() != 0U;
}

inline bool OTGOpType(const nnvm::NodeAttrs &attrs,
                           std::vector<int> *in_attrs,
                           std::vector<int> *out_attrs)
{
  CHECK_EQ(in_attrs->size(), 1U);
  CHECK_EQ(out_attrs->size(), 1U);

  out_attrs->at(0) = 3;        //KUint8=3
  return in_attrs->at(0) == 0; //KFloat32 = 0
}
inline bool OTGROpType(const nnvm::NodeAttrs &attrs,
                            std::vector<int> *in_attrs,
                            std::vector<int> *out_attrs)
{
  CHECK_EQ(in_attrs->size(), 1U);
  CHECK_EQ(out_attrs->size(), 1U);
  out_attrs->at(0) = 0;        //KFloat32 = 0
  return in_attrs->at(0) == 3; //KUint8=3
}

#define to_array(obj, to_type, from_type) ((to_type *)(obj.dptr<from_type>()))

template <typename xpu, typename policy_t>
void OTGROpForward_gpu(const nnvm::NodeAttrs &attrs,
                            const OpContext &ctx,
                            const std::vector<TBlob> &inputs,
                            const std::vector<OpReqType> &req,
                            const std::vector<TBlob> &outputs)
{
  const OTGRParam &param = nnvm::get<OTGRParam>(attrs.parsed);
  CHECK_EQ(inputs.size(), 1U);
  CHECK_EQ(outputs.size(), 1U);
  CHECK_EQ(req.size(), 1U);

  const TBlob &in_data = inputs[0];
  const TBlob &out_data = outputs[0];
  auto in_uint8_t = to_array(in_data, uint8_t, uint8_t);
  auto out_float = to_array(out_data, float, float);
  int ret = original_terngrad::OriginalTernGradRBody(
    in_uint8_t,
    in_data.Size(),
    out_float,
    out_data.Size()
  );
  if (unlikely(ret != 0))
  {
    printf("OTGROpForward_gpu failed! Error Code = %d\n", ret);
    CHECK_EQ(0, 1);
  }
}

template <typename xpu, typename policy_t>
void OTGOpForward_gpu(const nnvm::NodeAttrs &attrs,
                           const OpContext &ctx,
                           const std::vector<TBlob> &inputs,
                           const std::vector<OpReqType> &req,
                           const std::vector<TBlob> &outputs)
{
  const OTGParam &param = nnvm::get<OTGParam>(attrs.parsed);
  CHECK_EQ(inputs.size(), 1U);
  CHECK_EQ(outputs.size(), 1U);
  CHECK_EQ(req.size(), 1U);
  const TBlob &in_data = inputs[0];
  const TBlob &out_data = outputs[0];
  auto in_float = to_array(in_data, float, float);
  auto out_uint8_t = to_array(out_data, uint8_t, uint8_t);
  int ret = original_terngrad::OriginalTernGradBody(
    in_float,
    in_data.Size(),
    out_uint8_t,
    out_data.Size(),
    param.bitwidth,
    param.random
  );
  if (unlikely(ret != 0))
  {
    printf("TerngardOpForward_gpu failed! Error Code = %d\n", ret);
    CHECK_EQ(0, 1);
  };
}

} // namespace op
} // namespace mxnet

#endif // MXNET_OPERATOR_CONTRIB_OTG_OP_INL_H_
