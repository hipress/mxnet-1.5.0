#include "./generate_random-inl.h"
#include <thrust/execution_policy.h>
#include <thrust/system/omp/execution_policy.h>


namespace mxnet {
namespace op {

DMLC_REGISTER_PARAMETER(floatRandomParam);

NNVM_REGISTER_OP(_contrib_float_random)
.set_attr_parser(ParamParser<floatRandomParam>)
.set_num_inputs(0)
.set_num_outputs(1)
.set_attr<mxnet::FInferShape>("FInferShape", floatRandomShape)
.set_attr<nnvm::FInferType>("FInferType", floatRandomType)
.set_attr<nnvm::FInplaceOption>("FInplaceOption",
  [](const NodeAttrs& attrs) {
    return std::vector<std::pair<int, int> >{{0, 0}};
  })
.add_arguments(floatRandomParam::__FIELDS__())
;




NNVM_REGISTER_OP(_contrib_float_random)
.set_attr<FCompute>("FCompute<cpu>", floatRandomImpl<cpu,thrust::detail::host_t>)
;

}
}