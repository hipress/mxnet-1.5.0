/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/*!
 * \file quadratic_op.cc
 * \brief CPU Implementation of quadratic op
 */
#include "./otg-inl.h"
#include <thrust/execution_policy.h>  //thrust::device
#include <thrust/system/omp/execution_policy.h>

namespace mxnet {
namespace op {

DMLC_REGISTER_PARAMETER(OTGParam);
DMLC_REGISTER_PARAMETER(OTGRParam);

NNVM_REGISTER_OP(_contrib_OTG)
.set_attr_parser(ParamParser<OTGParam>)
.set_num_inputs(1)
.set_num_outputs(1)
.set_attr<nnvm::FListInputNames>("FListInputNames",
  [](const NodeAttrs& attrs) {
    return std::vector<std::string>{"data"};
  })
.set_attr<mxnet::FInferShape>("FInferShape", OTGOpShape)
.set_attr<nnvm::FInferType>("FInferType", OTGOpType)
.set_attr<nnvm::FInplaceOption>("FInplaceOption",
  [](const NodeAttrs& attrs) {
    return std::vector<std::pair<int, int> >{{0, 0}};
  })
.add_argument("data", "NDArray-or-Symbol", "Input ndarray")
.add_arguments(OTGParam::__FIELDS__())
;



NNVM_REGISTER_OP(_contrib_OTGr)
.set_attr_parser(ParamParser<OTGRParam>)
.set_num_inputs(1)
.set_num_outputs(1)
.set_attr<nnvm::FListInputNames>("FListInputNames",
  [](const NodeAttrs& attrs) {
    return std::vector<std::string>{"data"};
  })
.set_attr<mxnet::FInferShape>("FInferShape", OTGROpShape)
.set_attr<nnvm::FInferType>("FInferType", OTGROpType)
.set_attr<nnvm::FInplaceOption>("FInplaceOption",
  [](const NodeAttrs& attrs) {
    return std::vector<std::pair<int, int> >{{0, 0}};
  })
.add_argument("data", "NDArray-or-Symbol", "Input ndarray")
.add_arguments(OTGRParam::__FIELDS__())
;

NNVM_REGISTER_OP(_contrib_OTGr)
.set_attr<FCompute>("FCompute<cpu>", OTGROpForward_gpu<cpu,thrust::detail::host_t>)
;

NNVM_REGISTER_OP(_contrib_OTG)
.set_attr<FCompute>("FCompute<cpu>", OTGOpForward_gpu<cpu,thrust::detail::host_t>)
;


NNVM_REGISTER_OP(_contrib_OTG_omp)
.set_attr_parser(ParamParser<OTGParam>)
.set_num_inputs(1)
.set_num_outputs(1)
.set_attr<nnvm::FListInputNames>("FListInputNames",
  [](const NodeAttrs& attrs) {
    return std::vector<std::string>{"data"};
  })
.set_attr<mxnet::FInferShape>("FInferShape", OTGOpShape)
.set_attr<nnvm::FInferType>("FInferType", OTGOpType)
.set_attr<nnvm::FInplaceOption>("FInplaceOption",
  [](const NodeAttrs& attrs) {
    return std::vector<std::pair<int, int> >{{0, 0}};
  })
.add_argument("data", "NDArray-or-Symbol", "Input ndarray")
.add_arguments(OTGParam::__FIELDS__())
;



NNVM_REGISTER_OP(_contrib_OTGr_omp)
.set_attr_parser(ParamParser<OTGRParam>)
.set_num_inputs(1)
.set_num_outputs(1)
.set_attr<nnvm::FListInputNames>("FListInputNames",
  [](const NodeAttrs& attrs) {
    return std::vector<std::string>{"data"};
  })
.set_attr<mxnet::FInferShape>("FInferShape", OTGROpShape)
.set_attr<nnvm::FInferType>("FInferType", OTGROpType)
.set_attr<nnvm::FInplaceOption>("FInplaceOption",
  [](const NodeAttrs& attrs) {
    return std::vector<std::pair<int, int> >{{0, 0}};
  })
.add_argument("data", "NDArray-or-Symbol", "Input ndarray")
.add_arguments(OTGRParam::__FIELDS__())
;

NNVM_REGISTER_OP(_contrib_OTGr_omp)
.set_attr<FCompute>("FCompute<cpu>", OTGROpForward_gpu<cpu,thrust::system::omp::detail::par_t>)
;

NNVM_REGISTER_OP(_contrib_OTG_omp)
.set_attr<FCompute>("FCompute<cpu>", OTGOpForward_gpu<cpu,thrust::system::omp::detail::par_t>)
;

}  // namespace op
}  // namespace mxnet
