/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/*!
 * \file quadratic_op.cc
 * \brief CPU Implementation of quadratic op
 */
#include "./dgc_op-inl.h"

namespace mxnet {
namespace op {

DMLC_REGISTER_PARAMETER(DGCParam);
DMLC_REGISTER_PARAMETER(DGCRParam);

NNVM_REGISTER_OP(_contrib_dgcr)
.set_attr_parser(ParamParser<DGCRParam>)
.set_num_inputs(1)
.set_num_outputs(1)
.set_attr<nnvm::FListInputNames>("FListInputNames",
  [](const NodeAttrs& attrs) {
    return std::vector<std::string>{"data"};
  })
.set_attr<mxnet::FInferShape>("FInferShape", DGCROpShape)
.set_attr<nnvm::FInferType>("FInferType", DGCROpType)
.set_attr<FCompute>("FCompute<cpu>", DGCROpForward<cpu>)
.set_attr<nnvm::FInplaceOption>("FInplaceOption",
  [](const NodeAttrs& attrs) {
    return std::vector<std::pair<int, int> >{{0, 0}};
  })
.add_argument("data", "NDArray-or-Symbol", "Input ndarray")
.add_arguments(DGCRParam::__FIELDS__())
;


NNVM_REGISTER_OP(_contrib_dgc)
.set_attr_parser(ParamParser<DGCParam>)
.set_num_inputs(1)
.set_num_outputs(1)
.set_attr<nnvm::FListInputNames>("FListInputNames",
  [](const NodeAttrs& attrs) {
    return std::vector<std::string>{"data"};
  })
.set_attr<mxnet::FInferShape>("FInferShape", DGCOpShape)
.set_attr<nnvm::FInferType>("FInferType", DGCOpType)
.set_attr<FCompute>("FCompute<cpu>", DGCOpForward<cpu>)
.set_attr<nnvm::FInplaceOption>("FInplaceOption",
  [](const NodeAttrs& attrs) {
    return std::vector<std::pair<int, int> >{{0, 0}};
  })
.add_argument("data", "NDArray-or-Symbol", "Input ndarray")
.add_arguments(DGCParam::__FIELDS__())
;



}  // namespace op
}  // namespace mxnet
