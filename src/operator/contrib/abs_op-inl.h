/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
  * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/*!
 * \file quad_function-inl.h
 * \brief Operator implementing quadratic function.
 * For using as an example in the tutorial of adding operators
 * in MXNet backend.
 */
#ifndef MXNET_OPERATOR_CONTRIB_ABS_OP_INL_H_
#define MXNET_OPERATOR_CONTRIB_ABS_OP_INL_H_

#include <mxnet/operator_util.h>
#include <vector>
#include "../mshadow_op.h"
#include "../mxnet_op.h"
#include "../operator_common.h"
#include "../elemwise_op_common.h"
#include "../tensor/init_op.h"
#include "../../../3rdparty/dmlc-core/include/dmlc/omp.h"

#include <iostream>
#include <chrono>

namespace mxnet {
namespace op {

struct AbsParam : public dmlc::Parameter<AbsParam> {
	// abs has no parameter
};

inline bool AbsOpShape(const nnvm::NodeAttrs& attrs,
                             mxnet::ShapeVector* in_attrs,
                             mxnet::ShapeVector* out_attrs) {
  std::cout<<"SHAPE:\tin_attrs->size():"<<in_attrs->size()<<std::endl;                               
  CHECK_EQ(in_attrs->size(), 1U);

  SHAPE_ASSIGN_CHECK(*out_attrs, 0, in_attrs->at(0));
  SHAPE_ASSIGN_CHECK(*in_attrs, 0, out_attrs->at(0));
  /*
  int cnt = 3;
  std::cout<<"out_attrs->at(0).ndim()="<<out_attrs->at(0).ndim()<<std::endl;
  std::cout<<"out_attrs->at(0).Size()="<<out_attrs->at(0).Size()<<std::endl;
  for (auto i =0; i<cnt; i++){
	  std::cout<<"in_attrs->at(0)["<<i<<"]="<<in_attrs->at(0)[i]<<std::endl;
  };
  for (auto i =0; i<cnt; i++){
	  std::cout<<"out_attrs->at(0)["<<i<<"]="<<out_attrs->at(0)[i]<<std::endl;
  };
  */
  return out_attrs->at(0).ndim() != 0U && out_attrs->at(0).Size() != 0U;
}

inline bool AbsOpType(const nnvm::NodeAttrs& attrs,
                            std::vector<int>* in_attrs,
                            std::vector<int>* out_attrs) {
  std::cout<<"TYPE:\tin_attrs->size():"<<in_attrs->size()<<std::endl;                               
  CHECK_EQ(out_attrs->size(), 1U);

  TYPE_ASSIGN_CHECK(*out_attrs, 0, in_attrs->at(0));
  TYPE_ASSIGN_CHECK(*in_attrs, 0, out_attrs->at(0));
  /*
  std::cout<<"in_attrs->at(0)="<<in_attrs->at(0)<<std::endl;
  std::cout<<"out_attrs->at(0)="<<out_attrs->at(0)<<std::endl;
  */
  return out_attrs->at(0) != -1;
}

template<int req>
struct abs_forward {
  template<typename DType>
  MSHADOW_XINLINE static void Map(int i, DType* out_data, const DType* in_data) {
    size_t j = i;
    if (in_data[i]>out_data[j]){
      out_data[j] = in_data[i];
    };
  }
};

#define to_array(obj,to_type,from_type) ((to_type*)(obj.dptr<from_type>()))



template<typename xpu>
void AbsOpForward(const nnvm::NodeAttrs& attrs,
                        const OpContext& ctx,
                        const std::vector<TBlob>& inputs,
                        const std::vector<OpReqType>& req,
                        const std::vector<TBlob>& outputs) {
  CHECK_EQ(inputs.size(), 1U);
  CHECK_EQ(outputs.size(), 1U);
  CHECK_EQ(req.size(), 1U);
  //mshadow::Stream<xpu> *s = ctx.get_stream<xpu>();
  const TBlob& in_data = inputs[0];
  //const TBlob& out_data = outputs[0];
  using namespace mxnet_op;

  auto in_float = to_array(in_data,float,float);
  //omp_set_num_threads(16); // set manually
  std::cout<<"start finding max value using openmp"<<std::endl;
  std::cout<<"max_threads:"<<omp_get_max_threads()<<std::endl;
  float max_val=9999999999999999;
  float min_val = -max_val;
#pragma omp parallel for reduction(max:max_val) reduction(min:min_val)
  for (auto i = 0; i < in_data.Size(); i++){
      max_val = in_float[i]>max_val?in_float[i]:max_val;
      min_val = in_float[i]<min_val?in_float[i]:min_val;
  };

  /*
  std::cout<<"thread_num:"<<omp_get_thread_num()<<"\tmax_threads:"<<omp_get_max_threads()<<std::endl;
  MSHADOW_TYPE_SWITCH(out_data.type_flag_,DType,{
    MXNET_ASSIGN_REQ_SWITCH(req[0],req_type,{
      Kernel<all_gather<req_type>, xpu>::Launch(
          s, omp_get_max_threads(), out_data.dptr<DType>(), in_data.dptr<DType>()
          );
    };)
  });
  */
}


}  // namespace op
}  // namespace mxnet

#endif  // MXNET_OPERATOR_CONTRIB_ABS_OP_INL_H_
