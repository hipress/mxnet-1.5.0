#ifndef MXNET_OPERATOR_CONTRIB_GENERATE_RANDOM_INL_H_
#define MXNET_OPERATOR_CONTRIB_GENERATE_RANDOM_INL_H_

#include <mxnet/operator_util.h>
#include <vector>
#include "../mshadow_op.h"
#include "../mxnet_op.h"
#include "../operator_common.h"
#include "../elemwise_op_common.h"
#include "../tensor/init_op.h"
#include "get_policy-inl.h"

#include <thrust/transform.h> //trnasform
#include <thrust/iterator/counting_iterator.h>  // counting_iterator
#include <thrust/random.h>

#include <chrono>


namespace mxnet{
namespace op{
struct floatRandomParam : public dmlc::Parameter<floatRandomParam>{
  float param1;
  float param2;
  int type;
  DMLC_DECLARE_PARAMETER(floatRandomParam)
  {
    DMLC_DECLARE_FIELD(param1)
      .set_default(0.0f)
      .describe("lower bound, included")
      ;
    DMLC_DECLARE_FIELD(param2)
      .set_default(1.0f)
      .describe("upper bound, included/excluded?")
      ;
    DMLC_DECLARE_FIELD(type)
      .set_default(0)
      .describe("0: uniform_distribution, param1: lowerbound, param2: upperbound\
        1: normal_distribution, param1: mean, param2: standard deviation")
      ;
  };
};

inline bool floatRandomShape(
  const nnvm::NodeAttrs& attrs,
  mxnet::ShapeVector* in_attrs,
  mxnet::ShapeVector* out_attrs
){
  CHECK_EQ(out_attrs->size(),1U);
  return true;
}

inline bool floatRandomType(
  const nnvm::NodeAttrs& attrs,
  std::vector<int>* in_attrs,
  std::vector<int>* out_attrs
){
  CHECK_EQ(out_attrs->at(0),0U) << "output should be float32 type";
  return true;
}

struct uniform_float_random_generator{
  float param1,param2;
  unsigned long long t;
  uniform_float_random_generator(
    float _param1,
    float _param2,
    unsigned long long _t
  )
  : param1(_param1), param2(_param2), t(_t)
  {}
  __host__ __device__
  float operator()(const int32_t&i) const{
    thrust::default_random_engine rng;
    thrust::uniform_real_distribution<float> dist(param1,param2);  //[param1, param2)
    rng.discard(t+i);
    return dist(rng);
  }
};
struct normal_float_random_generator{
  float param1,param2;
  unsigned long long t;
  normal_float_random_generator(
    float _param1,
    float _param2,
    unsigned long long _t
  )
  : param1(_param1), param2(_param2), t(_t)
  {}
  __host__ __device__
  float operator()(const int32_t&i) const{
    thrust::default_random_engine rng;
    thrust::normal_distribution<float> dist(param1,param2);  //[param1, param2)
    rng.discard(t+i);
    return dist(rng);
  }
};


template<typename xpu, typename policy_t>
void floatRandomImpl(
  const nnvm::NodeAttrs& attrs,
  const OpContext& ctx,
  const std::vector<TBlob>&inputs,
  const std::vector<OpReqType>&req,
  const std::vector<TBlob>&outputs
){
  using namespace mxnet_op;
  const floatRandomParam& param = nnvm::get<floatRandomParam>(attrs.parsed);

  const TBlob& out_data = outputs[0];
  auto out_float = to_array(out_data, float, float);
  if (param.type==0){
    thrust::transform(
      get_policy<policy_t>::get(ctx),
      thrust::counting_iterator<int32_t>(0),
      thrust::counting_iterator<int32_t>(out_data.Size()),
      out_float,
      uniform_float_random_generator(
        param.param1,
        param.param2,
        static_cast<unsigned long long>(
          std::chrono::high_resolution_clock::now()
          .time_since_epoch()
          .count()
        )
      )
    );
  }
  else if (param.type==1){
    thrust::transform(
      get_policy<policy_t>::get(ctx),
      thrust::counting_iterator<int32_t>(0),
      thrust::counting_iterator<int32_t>(out_data.Size()),
      out_float,
      normal_float_random_generator(
        param.param1,
        param.param2,
        static_cast<unsigned long long>(
          std::chrono::high_resolution_clock::now()
          .time_since_epoch()
          .count()
        )
      )
    );
  }
}

} // namespace op
} // namespace mxnet

#endif
