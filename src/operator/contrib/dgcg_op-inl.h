/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/*!
 * \file quad_function-inl.h
 * \brief Operator implementing quadratic function.
 * For using as an example in the tutorial of adding operators
 * in MXNet backend.
 */
#ifndef MXNET_OPERATOR_CONTRIB_DGCG_INL_H_
#define MXNET_OPERATOR_CONTRIB_DGCG_INL_H_

#include <mxnet/operator_util.h>
#include <vector>
#include "../mshadow_op.h"
#include "../mxnet_op.h"
#include "../operator_common.h"
#include "../elemwise_op_common.h"
#include "../tensor/init_op.h"

#include <iostream>
#include <chrono>
#include <random>
#include <cmath> //abs
#include <algorithm> // sort



namespace mxnet {
namespace op {



struct DGCGParam : public dmlc::Parameter<DGCGParam> {
  float s_percent;
  float sample_rate;
  int result_num_per_thread;
  DMLC_DECLARE_PARAMETER(DGCGParam){
    DMLC_DECLARE_FIELD(s_percent)
      .set_default(0.01)
      .describe("Range of values:(0,1], determines how many values should be sent out");
    DMLC_DECLARE_FIELD(sample_rate)
      .set_default(0.001)
      .describe("input.size * sample_rate = sample_cnt");
    DMLC_DECLARE_FIELD(result_num_per_thread)
      .set_default(10)
      .describe(R"(When filtering: each thread produce 10[default] result.
      if true num of results is less than 10[default], fill with 0.
      if true num of results is more than 10[default], extra parts will be abandoned.)");
  };
};
struct DGCRGParam : public dmlc::Parameter<DGCRGParam> {
  int original_size;
  int quantized_num;
  DMLC_DECLARE_PARAMETER(DGCRGParam){
    DMLC_DECLARE_FIELD(original_size)
      .describe("Indicates [number] of origin uncompressed data.");
    DMLC_DECLARE_FIELD(quantized_num)
      .set_default(0)
      .describe("Indicates [number] of compressed data. The number is 1/2 of the size.");
  };
};


inline bool DGCGOpShape(const nnvm::NodeAttrs& attrs,
                             mxnet::ShapeVector* in_attrs,
                             mxnet::ShapeVector* out_attrs) {
  CHECK_EQ(in_attrs->size(), 1U);
  CHECK_EQ(out_attrs->size(), 1U);

  const DGCGParam& param = nnvm::get<DGCGParam>(attrs.parsed);
  //used for storing results
  size_t min_output_size_by_s_percent = static_cast<size_t>(in_attrs->at(0)[0] * (param.s_percent + 1e-6));
  min_output_size_by_s_percent = 
    (min_output_size_by_s_percent + param.result_num_per_thread - 1)
    / param.result_num_per_thread // get ceil(min_output_size_by_sample_rate / param.result_num_per_thread)
    * param.result_num_per_thread
    * 2 // size is doube of num of datas: one for float value, the other for int32_t index;
    + 1 // extend one for give output_size
    ;
  //used for sampling 
  //4 for sort
  size_t min_output_size_by_sample_rate = static_cast<size_t>(in_attrs->at(0)[0] * param.sample_rate + 1024)*4;
    
  size_t min_output_size = std::max(min_output_size_by_s_percent,min_output_size_by_sample_rate);

  if (out_attrs->at(0).ndim()==0U){
    out_attrs->at(0) = in_attrs->at(0); // copy ndim
    out_attrs->at(0)[0] = min_output_size;
  }
  else{
    CHECK_GE(out_attrs->at(0)[0], min_output_size);
  };
  return true;
}
inline bool DGCRGOpShape(const nnvm::NodeAttrs& attrs,
                             mxnet::ShapeVector* in_attrs,
                             mxnet::ShapeVector* out_attrs) {
  CHECK_EQ(in_attrs->size(), 1U);
  CHECK_EQ(out_attrs->size(), 1U);

  const DGCRGParam& param = nnvm::get<DGCRGParam>(attrs.parsed);
  CHECK_GE(in_attrs->at(0)[0], param.quantized_num);
  int32_t output_size = param.original_size;

  if (out_attrs->at(0).ndim()==0U){
    out_attrs->at(0) = in_attrs->at(0); // copy ndim
    out_attrs->at(0)[0] = output_size;
  }
  else{
    CHECK_EQ(out_attrs->at(0)[0],output_size);
  };
  return true;
}

inline bool DGCGOpType(const nnvm::NodeAttrs& attrs,
                            std::vector<int>* in_attrs,
                            std::vector<int>* out_attrs) {
  CHECK_EQ(in_attrs->size(), 1U);
  CHECK_EQ(out_attrs->size(), 1U);

  out_attrs->at(0) = 0; //KFloat32=0, could transfer to int by (uint32_t)
  return in_attrs->at(0) == 0; //original data, should be float 32 type
}

inline bool DGCRGOpType(const nnvm::NodeAttrs& attrs,
                            std::vector<int>* in_attrs,
                            std::vector<int>* out_attrs) {
  CHECK_EQ(in_attrs->size(), 1U);
  CHECK_EQ(out_attrs->size(), 1U);

  out_attrs->at(0) = 0; //KFloat32=0, could transfer to int by (uint32_t)
  return in_attrs->at(0) == 0; //quantized data, should be float 32 type
}

}  // namespace op
}  // namespace mxnet

#endif  // MXNET_OPERATOR_CONTRIB_DGCG_OP_INL_H_
